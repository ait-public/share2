declare const _default: {
    dict: {
        schemaName: string;
        remotes: ({
            remoteTypeName: string;
            remoteTypePkName: string;
            remoteTypeFunc?: undefined;
            remoteTypeFuncArgs?: undefined;
        } | {
            remoteTypeName: string;
            remoteTypePkName: string;
            remoteTypeFunc: string;
            remoteTypeFuncArgs: (ids: any) => object;
        })[];
    };
    person: {
        schemaName: string;
        remotes: {
            remoteTypeName: string;
            remoteTypePkName: string;
        }[];
    };
};
export default _default;
