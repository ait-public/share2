export declare const defaultRemoteTypeFuncArgs: (ids: any[], remoteTypePkName: string) => object;
declare const _default: {
    schemaName: string;
    remotes: ({
        remoteTypeName: string;
        remoteTypePkName: string;
        remoteTypeFunc?: undefined;
        remoteTypeFuncArgs?: undefined;
    } | {
        remoteTypeName: string;
        remoteTypePkName: string;
        remoteTypeFunc: string;
        remoteTypeFuncArgs: (ids: any) => object;
    })[];
};
export default _default;
