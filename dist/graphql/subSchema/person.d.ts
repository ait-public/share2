declare const _default: {
    schemaName: string;
    remotes: {
        remoteTypeName: string;
        remoteTypePkName: string;
    }[];
};
export default _default;
