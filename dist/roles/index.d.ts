export declare const ANONYM: {
    id: string;
    code: string;
    code2: string;
    name: string;
    names: string;
    order: number;
};
export declare const USER: {
    id: string;
    code: string;
    code2: string;
    name: string;
    names: string;
    order: number;
};
export declare const PATIENT: {
    id: string;
    code: string;
    code2: string;
    name: string;
    names: string;
    order: number;
};
export declare const EMPLOYEE: {
    id: string;
    code: string;
    code2: string;
    name: string;
    names: string;
    order: number;
};
export declare const ADMIN: {
    id: string;
    code: string;
    code2: string;
    name: string;
    names: string;
    order: number;
};
export declare const SYSADMIN: {
    id: string;
    code: string;
    code2: string;
    name: string;
    names: string;
    order: number;
};
export declare const SYSTEM: {
    id: string;
    code: string;
    code2: string;
    name: string;
    names: string;
    order: number;
};
export interface IRole {
    id: string;
    code: string;
    code2: string;
    name: string;
    names: string;
    order: number;
}
export declare const ROLES: Map<string, IRole>;
export declare function findRole(nameOrId: string): IRole;
export declare function findRoles(nameOrIds: string[]): IRole[];
export declare function finded(r: IRole, q?: string): boolean;
export declare function getList({ ids, q, where, }: {
    ids?: string[];
    q?: string;
    where?: {
        filter?: {
            roleOrLess?: string;
        };
    };
}): IRole[];
export declare function isSystem(role: string): boolean;
export declare function isSysadmin(role: string): boolean;
export declare function isAdmin(role: string): boolean;
export declare function isEmployee(role: string): boolean;
export declare function isPatient(role: string): boolean;
export declare function isUser(role: string): boolean;
export declare function isAnonym(role: string): boolean;
export declare function order(role: string): number;
export declare function isAdminOrMore(role: string): boolean;
export declare function isEmployeeOrMore(role: string): boolean;
export declare function isPatientOrMore(role: string): boolean;
export declare function isUserOrMore(role: string): boolean;
export declare function isAdminOrLess(role: string): boolean;
export declare function isEmployeeOrLess(role: string): boolean;
export declare function isPatientOrLess(role: string): boolean;
export declare function isUserOrLess(role: string): boolean;
