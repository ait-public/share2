'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

function _interopDefault (ex) { return (ex && (typeof ex === 'object') && 'default' in ex) ? ex['default'] : ex; }

var format = _interopDefault(require('date-fns/format'));
var ru = _interopDefault(require('date-fns/locale/ru'));

function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
  try {
    var info = gen[key](arg);
    var value = info.value;
  } catch (error) {
    reject(error);
    return;
  }

  if (info.done) {
    resolve(value);
  } else {
    Promise.resolve(value).then(_next, _throw);
  }
}

function _asyncToGenerator(fn) {
  return function () {
    var self = this,
        args = arguments;
    return new Promise(function (resolve, reject) {
      var gen = fn.apply(self, args);

      function _next(value) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
      }

      function _throw(err) {
        asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
      }

      _next(undefined);
    });
  };
}

function _extends() {
  _extends = Object.assign || function (target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];

      for (var key in source) {
        if (Object.prototype.hasOwnProperty.call(source, key)) {
          target[key] = source[key];
        }
      }
    }

    return target;
  };

  return _extends.apply(this, arguments);
}

function _unsupportedIterableToArray(o, minLen) {
  if (!o) return;
  if (typeof o === "string") return _arrayLikeToArray(o, minLen);
  var n = Object.prototype.toString.call(o).slice(8, -1);
  if (n === "Object" && o.constructor) n = o.constructor.name;
  if (n === "Map" || n === "Set") return Array.from(o);
  if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen);
}

function _arrayLikeToArray(arr, len) {
  if (len == null || len > arr.length) len = arr.length;

  for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i];

  return arr2;
}

function _createForOfIteratorHelperLoose(o, allowArrayLike) {
  var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"];
  if (it) return (it = it.call(o)).next.bind(it);

  if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") {
    if (it) o = it;
    var i = 0;
    return function () {
      if (i >= o.length) return {
        done: true
      };
      return {
        done: false,
        value: o[i++]
      };
    };
  }

  throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method.");
}

function flattenArray(arr) {
  if (!Array.isArray(arr)) {
    return [];
  }

  return arr.reduce(function (flat, toFlatten) {
    return flat.concat(Array.isArray(toFlatten) ? flattenArray(toFlatten) : toFlatten);
  }, []);
}
function distinct(source) {
  return source.filter(function (value, index) {
    return source.indexOf(value) === index;
  });
}
function sortByIds(sourceIds, orderedIds) {
  if (Array.isArray(orderedIds) && orderedIds.length > 0 && Array.isArray(sourceIds) && sourceIds.length > 0) {
    var sortIds = distinct(orderedIds.filter(function (c) {
      return sourceIds.includes(c);
    }));
    sourceIds = [].concat(sortIds, sourceIds.filter(function (c) {
      return !sortIds.includes(c);
    }));
  }

  return sourceIds;
}
function toCamelCase(str) {
  return str.replace(/^([A-Z])|\s(\w)/g, function (_, p1, p2) {
    if (p2) {
      return p2.toUpperCase();
    }

    return p1.toLowerCase();
  });
}
function kebab_case(str) {
  return (str || '').replace(/[A-Z]+(?![a-z])|[A-Z]/g, function ($, ofs) {
    return (ofs ? '-' : '') + $.toLowerCase();
  });
} // var myStr = 'thisIsAString'; --> "this-is-a-string"
// https://gist.github.com/jonlabelle/5375315

function hyphenate(str) {
  return str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase();
}
function cloneByJson(obj) {
  if (obj === undefined) {
    return obj;
  }

  return JSON.parse(JSON.stringify(obj));
}
function traverse(f, node, prop) {
  var _extends2;

  if (prop === void 0) {
    prop = 'children';
  }

  return f(_extends({}, node, (_extends2 = {}, _extends2[prop] = traverseAll(f, node[prop], prop), _extends2)));
}
/**
 *
 * @param f map function
 * @param nodes array of nodes
 * @param prop property to child nodes
 * @example
 *   utils.traverseAll(
 *    (node: any) => ({ ...node, icon: "mdi-icon", disable: node.id === "2" }),
 *    [{ lev: "1", children: [{ lev: "2", id: "2" }] }]
 *   )
 */

function traverseAll(f, nodes, prop) {
  if (nodes === void 0) {
    nodes = [];
  }

  if (prop === void 0) {
    prop = 'children';
  }

  return nodes.map(function (node) {
    return traverse(f, node, prop);
  });
}
function isGuid(value) {
  if (!value) return false;
  var regex = /[a-f0-9]{8}(?:-[a-f0-9]{4}){3}-[a-f0-9]{12}/i;
  var match = regex.exec(value);
  return match != null;
}
function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0,
        v = c == 'x' ? r : r & 0x3 | 0x8;
    return v.toString(16);
  });
}

function toStr(v) {
  switch (typeof v) {
    case 'string':
    case 'boolean':
    case 'number':
      return v.toString();
  }

  if (v instanceof Date) return format(v, 'yyyyMMddHHmmss', {
    locale: ru
  });
  return null;
}

function isNullBlankOrUndefined(o) {
  return typeof o === 'undefined' || o === null || o === '';
}

function deepEqual(a, b, opts) {
  if (a === b) {
    return true;
  }

  if (!(opts != null && opts.isStrongVal)) {
    if (isNullBlankOrUndefined(a) && isNullBlankOrUndefined(b)) {
      return true;
    }

    var hasDate = a instanceof Date || b instanceof Date;

    if (hasDate) {
      var d1, d2;

      if (a instanceof Date) {
        d1 = toStr(a);
      } else if (isDate(a)) {
        var dt1 = new Date(a);
        if (a.length === 10) dt1 = trimDate(dt1, TrimDateOpts.HOUR);
        d1 = toStr(dt1);
      }

      if (b instanceof Date) {
        d2 = toStr(b);
      } else if (isDate(b)) {
        var dt2 = new Date(b);
        if (b.length === 10) dt2 = trimDate(dt2, TrimDateOpts.HOUR);
        d2 = toStr(dt2);
      }

      return d1 === d2;
    }

    var s1 = toStr(a);

    if (s1 !== null) {
      var s2 = toStr(b);

      if (s2 !== null) {
        return s1 === s2;
      }
    }
  }

  if (a instanceof Date && b instanceof Date) {
    // If the values are Date, they were convert to timestamp with getTime and compare it
    if (a.getTime() !== b.getTime()) {
      return false;
    }
  }

  if (a !== Object(a) || b !== Object(b)) {
    // If the values aren't objects, they were already checked for equality
    return false;
  }

  var propsA = Object.keys(a);
  var propsB = Object.keys(b);

  if (!(opts != null && opts.isStrongKey)) {
    if (propsB.length > propsA.length) return false;
    if (propsB.some(function (p) {
      return !propsA.includes(p);
    })) return false;
    propsA = propsB;
  } else if (propsA.length !== propsB.length) {
    // Different number of props, don't bother to check
    return false;
  }

  return propsA.every(function (p) {
    return deepEqual(a[p], b[p], opts);
  });
}
function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}
function getRandomString() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }

  return args[getRandomInt(0, args.length - 1)];
}
function debounce(fn, delay) {
  var timeoutId = 0;
  return function () {
    for (var _len2 = arguments.length, args = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
      args[_key2] = arguments[_key2];
    }

    clearTimeout(timeoutId);
    timeoutId = setTimeout(function () {
      return fn.apply(void 0, args);
    }, delay);
  };
}
var hasOwnProperty = Object.prototype.hasOwnProperty;
function has(object, key) {
  return object != null && hasOwnProperty.call(object, key);
} // export function throttle2<T extends (...args: any[]) => any>(fn: T, limit: number) {
//     let throttling = false;
//     return (...args: Parameters<T>): void | ReturnType<T> => {
//         if (!throttling) {
//             throttling = true;
//             setTimeout(() => throttling = false, limit);
//             return fn(...args);
//         }
//     };
// }

var isObject = function isObject(x) {
  return typeof x === 'object' && x !== null;
};
/**
 * 	Performs a deep merge of objects and returns new object. Does not modify
 * 	objects (immutable) and merges arrays via concatenation.
 *
 * @param objects - Objects to merge
 * @returns {object} New object with merged key/values
 */

function mergeDeep() {
  for (var _len3 = arguments.length, objects = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
    objects[_key3] = arguments[_key3];
  }

  var items = flattenArray(objects).filter(function (c) {
    return !!c;
  });
  return items.filter(function (c) {
    return !!c;
  }).reduce(function (prev, obj) {
    Object.keys(obj).forEach(function (key) {
      var pVal = prev[key];
      var oVal = obj[key];

      if (Array.isArray(pVal) && Array.isArray(oVal)) {
        prev[key] = distinct(pVal.concat.apply(pVal, oVal));
      } else if (isObject(pVal) && isObject(oVal)) {
        prev[key] = mergeDeep(pVal, oVal);
      } else {
        prev[key] = oVal;
      }
    });
    return prev;
  }, {});
} /// <summary>
///  Регулярное выражение для "день (год)", "дней (лет)", "дня (года)"
/// </summary>
/// <param name="form1">день</param>
/// <param name="form2">дней</param>
/// <param name="form3">дня</param>

/**
 * Регулярное выражение для "год", "года", "лет" ("день", "дня", "дня")
 * @param digit 23
 * @param form1 день (год)
 * @param form2 дня (года)
 * @param form3 дней (лет)
 * @param noDigit show number
 * @returns 23 дня
 */

function getForm(digit, form1, form2, form3, noDigit) {
  if (noDigit === void 0) {
    noDigit = false;
  }

  digit = parseInt(digit, 10);

  if (isNaN(digit)) {
    return null;
  }

  if (digit < 0) {
    digit = -1 * digit;
  }

  var digitNew = digit % 100;
  var s = noDigit ? '' : digit + ' ';
  var lastFigure = digitNew % 10;

  if (digitNew > 10 && digitNew < 20) {
    s += form3;
  } else if (lastFigure === 1) {
    s += form1;
  } else if (lastFigure > 1 && lastFigure < 5) {
    s += form2;
  } else {
    s += form3;
  }

  return s;
}
var TrimDateOpts;

(function (TrimDateOpts) {
  TrimDateOpts[TrimDateOpts["YEAR"] = 0] = "YEAR";
  TrimDateOpts[TrimDateOpts["MONTH"] = 1] = "MONTH";
  TrimDateOpts[TrimDateOpts["DAY"] = 2] = "DAY";
  TrimDateOpts[TrimDateOpts["HOUR"] = 3] = "HOUR";
  TrimDateOpts[TrimDateOpts["MIN"] = 4] = "MIN";
  TrimDateOpts[TrimDateOpts["SEC"] = 5] = "SEC";
  TrimDateOpts[TrimDateOpts["MSEC"] = 6] = "MSEC";
})(TrimDateOpts || (TrimDateOpts = {}));

function trimDate(d, opts) {
  if (d instanceof Date) {
    switch (opts) {
      case TrimDateOpts.YEAR:
        return new Date(0, 0, 1);

      case TrimDateOpts.MONTH:
        return new Date(d.getFullYear(), 0, 1);

      case TrimDateOpts.DAY:
        return new Date(d.getFullYear(), d.getMonth(), 1);

      case TrimDateOpts.HOUR:
        return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0);

      case TrimDateOpts.SEC:
        return new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours());

      case TrimDateOpts.MSEC:
        return new Date(d.getFullYear(), d.getMonth(), d.getDate(), d.getHours(), d.getMilliseconds());
    }
  }

  return undefined;
}
var NULL_DATE = /*#__PURE__*/new Date(0);
var NULL_DELPHI_DATE = /*#__PURE__*/new Date('1899-12-30T00:00:00');
var DATE_LEN_1 = '1967-03-31T03:00:00'.length;
function pack(obj, removeOpts) {
  var opts = removeOpts || {};

  if (obj === undefined) {
    return undefined;
  }

  if (opts.isNull && obj === null) {
    return undefined;
  }

  if (typeof obj === 'string') {
    if (opts.isTrimString) {
      obj = obj.trim();
    }

    if ((opts.isEmptyString || opts.isDefVals) && obj === '') {
      return undefined;
    }

    if (opts != null && opts.isParseToDate && obj && obj.length === DATE_LEN_1 && obj[4] === '-' && isDate(obj)) {
      obj = new Date(obj);
    } else {
      return obj;
    }
  }

  if (obj instanceof Date) {
    if (opts.isZeroDate || opts.isDefVals) {
      var t = obj.getTime();

      if (NULL_DATE.getTime() === t || NULL_DELPHI_DATE.getTime() === t) {
        return undefined;
      }
    }

    if (opts.trimDate) {
      return trimDate(obj, opts.trimDate);
    }

    return obj;
  }

  if (Number.isInteger(obj)) {
    if (opts.isDefVals) {
      if (obj === 0 || isNaN(obj)) {
        return undefined;
      }
    }

    return obj;
  }

  if (typeof obj === 'boolean') {
    if (opts.isDefVals && !obj) {
      return undefined;
    }

    return obj;
  }

  if (Array.isArray(obj)) {
    var array = obj.map(function (c) {
      return pack(c, opts);
    }).filter(function (c) {
      return c !== undefined;
    });

    if (opts.isEmptyArray && array.length === 0) {
      return undefined;
    }

    if (opts.isDistinctArray) {
      array = distinct(array);
    } //


    return array;
  }

  if (typeof obj === 'object') {
    if (obj === null) return null;
    var packObj = {};
    var isEmpty = true;

    for (var prop in obj) {
      if (opts.removeProps && opts.removeProps.includes(prop)) {
        continue;
      }

      if (has(obj, prop)) {
        var val = pack(obj[prop], opts);

        if (val !== undefined) {
          isEmpty = false;
          packObj[prop] = val;
        }
      }
    }

    if (opts.isEmptyObj && isEmpty) {
      return undefined;
    }

    return packObj;
  }

  return undefined;
}
function mergePackDeep(items, opts) {
  items = (items || []).filter(function (c) {
    return !!c;
  }).map(function (c) {
    return pack(c, opts);
  });
  return mergeDeep(items);
}
function isDate(value) {
  switch (typeof value) {
    case 'number':
      return true;

    case 'string':
      return !isNaN(Date.parse(value));

    case 'object':
      if (value instanceof Date) {
        return !isNaN(value.getTime());
      }

      break;
  }

  return false;
}
function formatDate(date, fmt) {
  if (fmt === void 0) {
    fmt = 'yyyy-MM-dd';
  }

  return isDate(date) ? format(new Date(date), fmt, {
    locale: ru
  }) : undefined;
}
function formatDateTime(date, fmt) {
  if (fmt === void 0) {
    fmt = "yyyy-MM-dd'T'HH:mm:ss";
  }

  return formatDate(date, fmt);
}
function fio(item, def) {
  if (def === void 0) {
    def = null;
  }

  if (!item) return def;
  var firstname = item.firstname || '';
  var surname = item.surname || '';
  var patronymic = item.patronymic || '';
  var arr = [surname, firstname, patronymic];
  var sfio = arr.filter(function (val) {
    return val;
  }).join(' ');
  if (sfio) sfio = sfio.trim();
  return sfio || def;
}
function initialFio(item, def) {
  if (def === void 0) {
    def = null;
  }

  if (!item) return def;
  var firstname = item.firstname && item.firstname.charAt(0) + '.' || '';
  var patronymic = item.patronymic && item.patronymic.charAt(0) + '.' || '';
  var surname = item.surname || '';
  var fio = surname + " " + firstname + patronymic;
  if (fio) fio = fio.trim();
  return fio || def;
}
function anonymFio(item, def) {
  if (def === void 0) {
    def = 'аноним';
  }

  return fio(item) || def;
}
function docSerNum(item, def) {
  if (def === void 0) {
    def = '-';
  }

  if (!item) return def;
  return ((item.series || '') + ' ' + (item.number || '')).trim() || def;
}
function ageYears(birthDate, otherDate) {
  if (!otherDate) otherDate = new Date();
  birthDate = new Date(birthDate);
  otherDate = new Date(otherDate);
  var years = otherDate.getFullYear() - birthDate.getFullYear();

  if (otherDate.getMonth() < birthDate.getMonth() || otherDate.getMonth() === birthDate.getMonth() && otherDate.getDate() < birthDate.getDate()) {
    years--;
  }

  return years;
}
function ageStr(age) {
  if (typeof age === 'string') {
    age = parseInt(age, 10);

    if (isNaN(age)) {
      return null;
    }
  }

  var txt;
  var count = age % 100;

  if (count >= 5 && count <= 20) {
    txt = 'лет';
  } else {
    count = count % 10;

    if (count === 1) {
      txt = 'год';
    } else if (count >= 2 && count <= 4) {
      txt = 'года';
    } else {
      txt = 'лет';
    }
  }

  return age + ' ' + txt;
}
function age(birthDate, otherDate) {
  return ageStr(ageYears(birthDate, otherDate));
}
/**
 * Replaces "" object prop values with undefined | null
 * @param {any} obj Object
 * @returns cleaned object
 */

function cleanEmptyProps(obj, replaceTo) {
  if (replaceTo === void 0) {
    replaceTo = undefined;
  }

  if (!obj) return obj;
  return Object.entries(obj).reduce(function (a, _ref) {
    var k = _ref[0],
        v = _ref[1];
    return v !== '' ? (a[k] = v, a) : (a[k] = replaceTo, a);
  }, {});
}
/**
 * Used in update section of graphql save operations
 * @param chgObj changed object
 * @param origObj original object
 * @param propName name of the object property
 * @returns set clause or undefined
 */

function updateField(chgObj, origObj, propName) {
  return (chgObj || {})[propName] !== (origObj || {})[propName] && {
    set: (chgObj || {})[propName] || null
  } || undefined;
}
function formatBytes(bytes, decimals) {
  if (decimals === void 0) {
    decimals = 2;
  }

  if (!bytes) return '0 Bytes';
  var k = 1024;
  var dm = decimals < 0 ? 0 : decimals;
  var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
  var i = Math.floor(Math.log(bytes) / Math.log(k));
  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}
var RE_YMD = /^\d{4}\-\d{1,2}\-\d{1,2}$/;
function isValidYMD(dateString) {
  // First check for the pattern
  if (!(dateString != null && dateString.length) || !RE_YMD.test(dateString)) {
    return false;
  } // Parse the date parts to integers


  var parts = dateString.split('-');
  var day = parseInt(parts[2], 10);
  var month = parseInt(parts[1], 10);
  var year = parseInt(parts[0], 10);
  return checkDMY(year, month, day);
}
var RE_DMY = /^\d{1,2}\.\d{1,2}\.\d{4}$/;
function isValidDMY(dateString) {
  // First check for the pattern
  if (!(dateString != null && dateString.length) || !RE_DMY.test(dateString)) {
    return false;
  } // Parse the date parts to integers


  var parts = dateString.split('.');
  var day = parseInt(parts[0], 10);
  var month = parseInt(parts[1], 10);
  var year = parseInt(parts[2], 10);
  return checkDMY(year, month, day);
}

function checkDMY(year, month, day) {
  // Check the ranges of month and year
  if (year < 1000 || year > 3000 || month === 0 || month > 12) {
    return false;
  }

  var monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31]; // Adjust for leap years

  if (year % 400 === 0 || year % 100 !== 0 && year % 4 === 0) {
    monthLength[1] = 29;
  } // Check the range of the day


  return day > 0 && day <= monthLength[month - 1];
}

function getPropValue(o, s) {
  s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties

  s = s.replace(/^\./, ''); // strip a leading dot

  var a = s.split('.');

  for (var i = 0, n = a.length; i < n; ++i) {
    var k = a[i];

    if (k in o) {
      o = o[k];
    } else {
      return;
    }
  }

  return o;
}
function search(items, searchQuery, searchProp) {
  if (!searchQuery) return items;
  var searchs = searchQuery.toLowerCase().split(' ');
  return items == null ? void 0 : items.filter(function (c) {
    return searchs.every(function (v) {
      return ((searchProp ? getPropValue(c, searchProp) : c) + '').toLowerCase().includes(v);
    });
  });
}
var reEmail = /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g;
var parseEmail = function parseEmail(str) {
  var t = str && str.trim().match(reEmail) || []; // if (t) return "mailto:" + t;

  return t.length ? t[0] : '';
};
/**
 * Returns a copy of an object with all keys sorted.
 * @param object target object
 * @param sortWith Its object containing ordered keys or a function to sort the keys (same signature as in Array.prototype.sort()).
 * @returns copy of an object with all keys sorted.
 * @example
 * sortObjectKeys({c:1, b:1, d:1, a:1}, {keys:['b', 'a', 'c']}) // => {b:1, a:1, c:1, d:1}
 * sortObjectKeys({c:1, b:1, d:1, a:1}, {keys:['b', 'a', 'c'], onlyKeys:true}) // => {b:1, a:1, c:1}
   sortObjectKeys({ "key-1": 1, "key-3": 1, "key-10": 1, "key-2": 1},
    {sortFn:(a, b)=>parseInt(a.slice(4))-parseInt(b.slice(4))}
   )
 */

function sortObjectKeys(object, sortWith) {
  if (!object) return object;
  var keys = (sortWith == null ? void 0 : sortWith.keys) || [];
  var sortFn = sortWith == null ? void 0 : sortWith.sortFn;
  var onlyKeys = !!(sortWith != null && sortWith.onlyKeys);
  var objectKeys = Object.keys(object);
  return keys.concat(objectKeys.sort(sortFn)).reduce(function (total, key) {
    if (objectKeys.indexOf(key) !== -1) {
      if (!onlyKeys || keys.includes(key)) {
        total[key] = object[key];
      }
    }

    return total;
  }, Object.create(null));
}

function _postOrder(root, parent, key, h) {
  if (root == null) return;

  if (Array.isArray(root)) {
    var len = root.length;

    for (var i = 0; i < len; i++) {
      var value = root[i];

      if (!h.forObj || value && typeof value === 'object') {
        var r = _postOrder(value, root, i, h);

        if (r !== undefined) return r;
      }
    }
  } else if (typeof root === 'object') {
    for (var _i = 0, _Object$entries = Object.entries(root); _i < _Object$entries.length; _i++) {
      var _Object$entries$_i = _Object$entries[_i],
          _key4 = _Object$entries$_i[0],
          _value = _Object$entries$_i[1];

      if (!h.forObj || _value && typeof _value === 'object') {
        var _r = _postOrder(_value, root, _key4, h);

        if (_r !== undefined) return _r;
      }
    }
  }

  return h.fn(root, parent, key);
}

function postOrder(root, fn, forObj) {
  if (forObj === void 0) {
    forObj = true;
  }

  var h = {
    fn: fn,
    forObj: forObj
  };
  return _postOrder(root, null, null, h);
}

function _preOrder(root, parent, key, h) {
  if (root == null) return;

  if (!h.forObj || root && typeof root === 'object') {
    var res = h.fn(root, parent, key);
    if (res !== undefined) return res;
  }

  if (Array.isArray(root)) {
    var len = root.length;

    for (var i = 0; i < len; i++) {
      var value = root[i];

      if (!h.forObj || value && typeof value === 'object') {
        var r = _preOrder(value, root, i, h);

        if (r !== undefined) return r;
      }
    }
  } else if (typeof root === 'object') {
    for (var _i2 = 0, _Object$entries2 = Object.entries(root); _i2 < _Object$entries2.length; _i2++) {
      var _Object$entries2$_i = _Object$entries2[_i2],
          _key5 = _Object$entries2$_i[0],
          _value2 = _Object$entries2$_i[1];

      if (!h.forObj || _value2 && typeof _value2 === 'object') {
        var _r2 = _preOrder(_value2, root, _key5, h);

        if (_r2 !== undefined) return _r2;
      }
    }
  }
}

function preOrder(root, fn, forObj) {
  if (forObj === void 0) {
    forObj = true;
  }

  var h = {
    fn: fn,
    forObj: forObj
  };
  return _preOrder(root, null, null, h);
}
function escapeHtml(unsafe) {
  if (typeof unsafe === 'string') {
    return unsafe && unsafe.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;').replace(/'/g, '&#039;');
  }

  return unsafe;
}
function sleep(ms) {
  return new Promise(function (resolve) {
    return setTimeout(resolve, ms);
  });
}
function getOnlyNums(val) {
  return (val + '').replace(/\D/g, '');
}
var fioRE = /^([ ]*[ёа-яЁА-Я][ ]*|[ ]*[ёа-яЁА-Я]+[ёа-яЁА-Я ]*[ёа-яЁА-Я\-ёа-яЁА-Я]?[ёа-яЁА-Я ]*[ёа-яЁА-Я]+[ ]*)$/;
function validFioRus(val) {
  return fioRE.test(val);
}
/*!
 * Find the differences between two objects and push to a new object
 * (c) 2019 Chris Ferdinandi & Jascha Brinkmann, MIT License, https://gomakethings.com & https://twitter.com/jaschaio
 * @param  {Object} obj1 The original object
 * @param  {Object} obj2 The object to compare against it
 * @return {Object}      An object of differences between the two
 */

function diff(obj1, obj2) {
  // Make sure an object to compare is provided
  if (!obj2 || Object.prototype.toString.call(obj2) !== '[object Object]') {
    return obj1;
  } // if (!obj1 && obj2) return obj2;
  //
  // Variables
  //


  var diffs = {};
  var key; //
  // Methods
  //

  /**
   * Check if two arrays are equal
   * @param  {Array}   arr1 The first array
   * @param  {Array}   arr2 The second array
   * @return {Boolean}      If true, both arrays are equal
   */

  var arraysMatch = function arraysMatch(arr1, arr2) {
    // Check if the arrays are the same length
    if (arr1.length !== arr2.length) return false; // Check if all items exist and are in the same order

    for (var i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) return false;
    } // Otherwise, return true


    return true;
  };
  /**
   * Compare two items and push non-matches to object
   * @param  {*}      item1 The first item
   * @param  {*}      item2 The second item
   * @param  {String} key   The key in our object
   */


  var compare = function compare(item1, item2, key) {
    // Get the object type
    var type1 = Object.prototype.toString.call(item1);
    var type2 = Object.prototype.toString.call(item2); // If type2 is undefined it has been removed

    if (type2 === '[object Undefined]') {
      diffs[key] = null;
      return;
    } // If items are different types


    if (type1 !== type2) {
      diffs[key] = item2;
      return;
    } // If an object, compare recursively


    if (type1 === '[object Object]') {
      var objDiff = diff(item1, item2);

      if (Object.keys(objDiff).length > 0) {
        diffs[key] = objDiff;
      }

      return;
    } // If an array, compare


    if (type1 === '[object Array]') {
      if (!arraysMatch(item1, item2)) {
        diffs[key] = item2;
      }

      return;
    } // Else if it's a function, convert to a string and compare
    // Otherwise, just compare


    if (type1 === '[object Function]') {
      if (item1.toString() !== item2.toString()) {
        diffs[key] = item2;
      }
    } else {
      if (item1 !== item2) {
        diffs[key] = item2;
      }
    }
  }; //
  // Compare our objects
  //
  // Loop through the first object


  for (key in obj1) {
    if (obj1.hasOwnProperty(key)) {
      compare(obj1[key], obj2[key], key);
    }
  } // Loop through the second object and find missing items


  for (key in obj2) {
    if (obj2.hasOwnProperty(key)) {
      if (!(obj1 != null && obj1[key]) && (obj1 == null ? void 0 : obj1[key]) !== obj2[key]) {
        diffs[key] = obj2[key];
      }
    }
  } // Return the object of differences


  return diffs;
}

function _isBuffer(obj) {
  return obj && obj.constructor && typeof obj.constructor.isBuffer === 'function' && obj.constructor.isBuffer(obj);
}

function _keyIdentity(key) {
  return key;
} // https://github.com/hughsk/flat

/**
 * @example
 * https://github.com/hughsk/flat
 */


function flatObject(target, opts) {
  opts = opts || {};
  var delimiter = opts.delimiter || '.';
  var maxDepth = opts.maxDepth || Number.MAX_VALUE;
  var transformKey = opts.transformKey || _keyIdentity;
  var output = {};

  function step(object, prev, currentDepth) {
    currentDepth = currentDepth || 1;
    Object.keys(object).forEach(function (key) {
      var _opts, _opts2;

      var value = object[key];
      var isarray = ((_opts = opts) == null ? void 0 : _opts.safe) && Array.isArray(value);
      var type = Object.prototype.toString.call(value);

      var isbuffer = _isBuffer(value);

      var isobject = type === '[object Object]' || type === '[object Array]';
      var newKey = prev ? prev + delimiter + transformKey(key) : transformKey(key);

      if (!isarray && !isbuffer && isobject && Object.keys(value).length && (!((_opts2 = opts) != null && _opts2.maxDepth) || currentDepth < maxDepth)) {
        return step(value, newKey, currentDepth + 1);
      }

      output[newKey] = value;
    });
  }

  step(target);
  return output;
}
function unflatObject(target, opts) {
  opts = opts || {};
  var delimiter = opts.delimiter || '.';
  var overwrite = opts.overwrite || false;
  var transformKey = opts.transformKey || _keyIdentity;
  var result = {};

  var isbuffer = _isBuffer(target);

  if (isbuffer || Object.prototype.toString.call(target) !== '[object Object]') {
    return target;
  } // safely ensure that the key is
  // an integer.


  function getkey(key) {
    var _opts3;

    var parsedKey = Number(key);
    return isNaN(parsedKey) || key.indexOf('.') !== -1 || (_opts3 = opts) != null && _opts3.object ? key : parsedKey;
  }

  function addKeys(keyPrefix, recipient, target) {
    return Object.keys(target).reduce(function (result, key) {
      result[keyPrefix + delimiter + key] = target[key];
      return result;
    }, recipient);
  }

  function isEmpty(val) {
    var type = Object.prototype.toString.call(val);
    var isArray = type === '[object Array]';
    var isObject = type === '[object Object]';

    if (!val) {
      return true;
    } else if (isArray) {
      return !(val != null && val.length);
    } else if (isObject) {
      var _Object$keys;

      return !((_Object$keys = Object.keys(val)) != null && _Object$keys.length);
    }

    return false;
  }

  target = Object.keys(target).reduce(function (result, key) {
    var type = Object.prototype.toString.call(target[key]);
    var isObject = type === '[object Object]' || type === '[object Array]';

    if (!isObject || isEmpty(target[key])) {
      result[key] = target[key];
      return result;
    } else {
      return addKeys(key, result, flatObject(target[key], opts));
    }
  }, {});
  Object.keys(target).forEach(function (key) {
    var split = key.split(delimiter).map(transformKey);
    var key1 = getkey(split.shift());
    var key2 = getkey(split[0]);
    var recipient = result;

    while (key2 !== undefined) {
      if (key1 === '__proto__') {
        return;
      }

      var type = Object.prototype.toString.call(recipient[key1]);
      var isobject = type === '[object Object]' || type === '[object Array]'; // do not write over falsey, non-undefined values if overwrite is false

      if (!overwrite && !isobject && typeof recipient[key1] !== 'undefined') {
        return;
      }

      if (overwrite && !isobject || !overwrite && recipient[key1] == null) {
        var _opts4;

        recipient[key1] = typeof key2 === 'number' && !((_opts4 = opts) != null && _opts4.object) ? [] : {};
      }

      recipient = recipient[key1];

      if (split.length > 0) {
        key1 = getkey(split.shift());
        key2 = getkey(split[0]);
      }
    } // unflatten again for 'messy objects'


    recipient[key1] = unflatObject(target[key], opts);
  });
  return result;
}
function capitalizeFirstLetter(str) {
  if (!(str != null && str.length)) return str;
  return str[0].toUpperCase() + str.slice(1);
}
function toSnakeCase(str) {
  var _str$match$map$join, _str$match;

  if (!str || !str.length) return str;
  var pattern = /[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g;
  return (_str$match$map$join = (_str$match = str.match(pattern)) == null ? void 0 : _str$match.map(function (x) {
    return x.toLowerCase();
  }).join('_')) != null ? _str$match$map$join : '';
}
function getId(entity, opts) {
  var _opts$defId;

  if (!entity) return undefined;
  var key = (_opts$defId = opts == null ? void 0 : opts.defId) != null ? _opts$defId : 'id';
  if (entity[key]) return entity[key];
  var Key = capitalizeFirstLetter(key);
  if (entity[Key]) return entity[Key];
  var key_id = toSnakeCase(key);
  if (entity[key_id]) return entity[key_id];
  var entityType = opts == null ? void 0 : opts.entityType;

  if (entityType) {
    var _entityType$split$;

    // service.type | type
    var ktype = entityType.includes('.') ? (_entityType$split$ = entityType.split('.')[1]) != null ? _entityType$split$ : '' : entityType;

    if (ktype) {
      var keyId = "" + ktype + Key;
      return getId(entity, {
        defId: keyId
      });
    }
  }

  return undefined;
} // export function flatObject(input: any, separator: string = '.') {
//   function flat(res: {}, key: string, val: { [x: string]: any; }, pre = ''): any {
//     const prefix = [pre, key].filter(v => v).join(separator);
//     return typeof val === 'object'
//       ? Object.keys(val).reduce((prev, curr) => flat(prev, curr, val[curr], prefix), res)
//       : Object.assign(res, { [prefix]: val });
//   }
//   return Object.keys(input).reduce((prev, curr) => flat(prev, curr, input[curr]), {});
// }

var index = {
  __proto__: null,
  flattenArray: flattenArray,
  distinct: distinct,
  sortByIds: sortByIds,
  toCamelCase: toCamelCase,
  kebab_case: kebab_case,
  hyphenate: hyphenate,
  cloneByJson: cloneByJson,
  traverse: traverse,
  traverseAll: traverseAll,
  isGuid: isGuid,
  uuidv4: uuidv4,
  deepEqual: deepEqual,
  getRandomInt: getRandomInt,
  getRandomString: getRandomString,
  debounce: debounce,
  has: has,
  isObject: isObject,
  mergeDeep: mergeDeep,
  getForm: getForm,
  get TrimDateOpts () { return TrimDateOpts; },
  trimDate: trimDate,
  NULL_DELPHI_DATE: NULL_DELPHI_DATE,
  pack: pack,
  mergePackDeep: mergePackDeep,
  isDate: isDate,
  formatDate: formatDate,
  formatDateTime: formatDateTime,
  fio: fio,
  initialFio: initialFio,
  anonymFio: anonymFio,
  docSerNum: docSerNum,
  ageYears: ageYears,
  ageStr: ageStr,
  age: age,
  cleanEmptyProps: cleanEmptyProps,
  updateField: updateField,
  formatBytes: formatBytes,
  isValidYMD: isValidYMD,
  isValidDMY: isValidDMY,
  getPropValue: getPropValue,
  search: search,
  parseEmail: parseEmail,
  sortObjectKeys: sortObjectKeys,
  postOrder: postOrder,
  preOrder: preOrder,
  escapeHtml: escapeHtml,
  sleep: sleep,
  getOnlyNums: getOnlyNums,
  validFioRus: validFioRus,
  diff: diff,
  flatObject: flatObject,
  unflatObject: unflatObject,
  capitalizeFirstLetter: capitalizeFirstLetter,
  toSnakeCase: toSnakeCase,
  getId: getId
};

var ANONYM = {
  id: 'ANONYM',
  code: 'anonym',
  code2: 'Аноним',
  name: 'Аноним',
  names: 'Анонимы',
  order: 0
};
var USER = {
  id: 'USER',
  code: 'user',
  code2: 'Пользователь',
  name: 'Пользователь',
  names: 'Пользователи',
  order: 5
};
var PATIENT = {
  id: 'PATIENT',
  code: 'patient',
  code2: 'Пациент',
  name: 'Пациент',
  names: 'Пациенты',
  order: 10
};
var EMPLOYEE = {
  id: 'EMPLOYEE',
  code: 'employee',
  code2: 'Сотрудник',
  name: 'Сотрудник',
  names: 'Сотрудники',
  order: 20
};
var ADMIN = {
  id: 'ADMIN',
  code: 'admin',
  code2: 'Админ',
  name: 'Администратор',
  names: 'Администраторы',
  order: 30
};
var SYSADMIN = {
  id: 'SYSADMIN',
  code: 'sysadmin',
  code2: 'Сисадмин',
  name: 'Системный администратор',
  names: 'Системные администраторы',
  order: 40
};
var SYSTEM = {
  id: 'SYSTEM',
  code: 'system',
  code2: 'Сервис',
  name: 'Системный сервис',
  names: 'Системные сервисы',
  order: 50
};
var ROLES = /*#__PURE__*/new Map([[ANONYM.id, ANONYM], [USER.id, USER], [PATIENT.id, PATIENT], [EMPLOYEE.id, EMPLOYEE], [ADMIN.id, ADMIN], [SYSADMIN.id, SYSADMIN], [SYSTEM.id, SYSTEM]]);

var _ROLES = /*#__PURE__*/Array.from( /*#__PURE__*/ROLES.values());

function findRole(nameOrId) {
  if (!nameOrId) return ANONYM;
  nameOrId = nameOrId.toUpperCase();

  if (ROLES.has(nameOrId)) {
    var r = ROLES.get(nameOrId);

    if (r) {
      return r;
    }
  }

  for (var _iterator = _createForOfIteratorHelperLoose(_ROLES), _step; !(_step = _iterator()).done;) {
    var value = _step.value;

    if (value.name.toUpperCase() === nameOrId || value.names.toUpperCase() === nameOrId) {
      return value;
    }
  }

  return ANONYM;
}
function findRoles(nameOrIds) {
  return nameOrIds.map(function (id) {
    return findRole(id);
  });
}
function finded(r, q) {
  if (!r) return false;
  if (!q) return true;
  q = q.toUpperCase();
  return r.code.toUpperCase().startsWith(q) || r.name.toUpperCase().startsWith(q) || r.names.toUpperCase().startsWith(q) || r.code2.toUpperCase().startsWith(q) ? true : false;
}
function getList(_ref) {
  var _where$filter;

  var ids = _ref.ids,
      q = _ref.q,
      where = _ref.where;
  var roleOrLess = where == null ? void 0 : (_where$filter = where.filter) == null ? void 0 : _where$filter.roleOrLess;
  var flist = ids ? findRoles(ids) : _ROLES;
  var result = flist.filter(function (c) {
    return finded(c, q);
  });
  if (!roleOrLess) return result;
  var r = findRole(roleOrLess);
  if (!r) return result;
  var rorder = r.order;
  return result.filter(function (c) {
    return c.order <= rorder;
  });
}
function isSystem(role) {
  return role === SYSTEM.id;
}
function isSysadmin(role) {
  return role === SYSADMIN.id;
}
function isAdmin(role) {
  return role === ADMIN.id;
}
function isEmployee(role) {
  return role === EMPLOYEE.id;
}
function isPatient(role) {
  return role === PATIENT.id;
}
function isUser(role) {
  return role === USER.id;
}
function isAnonym(role) {
  return findRole(role).id === ANONYM.id;
}
function order(role) {
  return findRole(role).order;
}
function isAdminOrMore(role) {
  return order(role) >= ADMIN.order;
}
function isEmployeeOrMore(role) {
  return order(role) >= EMPLOYEE.order;
}
function isPatientOrMore(role) {
  return order(role) >= PATIENT.order;
}
function isUserOrMore(role) {
  return order(role) >= USER.order;
}
function isAdminOrLess(role) {
  return order(role) <= ADMIN.order;
}
function isEmployeeOrLess(role) {
  return order(role) <= EMPLOYEE.order;
}
function isPatientOrLess(role) {
  return order(role) <= PATIENT.order;
}
function isUserOrLess(role) {
  return order(role) <= USER.order;
}

var index$1 = {
  __proto__: null,
  ANONYM: ANONYM,
  USER: USER,
  PATIENT: PATIENT,
  EMPLOYEE: EMPLOYEE,
  ADMIN: ADMIN,
  SYSADMIN: SYSADMIN,
  SYSTEM: SYSTEM,
  ROLES: ROLES,
  findRole: findRole,
  findRoles: findRoles,
  finded: finded,
  getList: getList,
  isSystem: isSystem,
  isSysadmin: isSysadmin,
  isAdmin: isAdmin,
  isEmployee: isEmployee,
  isPatient: isPatient,
  isUser: isUser,
  isAnonym: isAnonym,
  order: order,
  isAdminOrMore: isAdminOrMore,
  isEmployeeOrMore: isEmployeeOrMore,
  isPatientOrMore: isPatientOrMore,
  isUserOrMore: isUserOrMore,
  isAdminOrLess: isAdminOrLess,
  isEmployeeOrLess: isEmployeeOrLess,
  isPatientOrLess: isPatientOrLess,
  isUserOrLess: isUserOrLess
};

var EnumType = function EnumType(value) {
  this.$EnumType = true;
  this.value = value;
};

var VariableType = /*#__PURE__*/function () {
  function VariableType(value) {
    this.$VariableType = true;
    this.value = value;
  }

  var _proto = VariableType.prototype;

  _proto.toJSON = function toJSON() {
    return '$' + this.value;
  };

  return VariableType;
}();

var configFields = ['__args', '__alias', '__aliasFor', '__variables', '__directives', '__on', '__all_on', '__typeName'];

function stringify(obj_from_json) {
  if (obj_from_json instanceof EnumType || obj_from_json.$EnumType) {
    return obj_from_json.value;
  } else if (obj_from_json instanceof VariableType || obj_from_json.$VariableType) {
    return '$' + obj_from_json.value;
  } else if (typeof obj_from_json !== 'object' || obj_from_json === null) {
    return JSON.stringify(obj_from_json);
  } else if (Array.isArray(obj_from_json)) {
    return '[' + obj_from_json.map(function (item) {
      return stringify(item);
    }).join(', ') + ']';
  }

  var props = Object.keys(obj_from_json).map(function (key) {
    return key + ': ' + stringify(obj_from_json[key]);
  }).join(', ');
  return '{' + props + '}';
}

function buildArgs(argsObj) {
  var args = [];

  for (var argName in argsObj) {
    args.push(argName + ': ' + stringify(argsObj[argName]));
  }

  return args.join(', ');
}

function buildVariables(varsObj) {
  var args = [];

  for (var varName in varsObj) {
    args.push('$' + varName + ': ' + varsObj[varName]);
  }

  return args.join(', ');
}

function buildDirectives(dirsObj) {
  var directiveName = Object.keys(dirsObj)[0];
  var directiveValue = dirsObj[directiveName];

  if (typeof directiveValue === 'boolean') {
    return directiveName;
  } else if (typeof directiveValue === 'object') {
    var args = [];

    for (var argName in directiveValue) {
      var argVal = stringify(directiveValue[argName]).replace(/"/g, '');
      args.push(argName + ': ' + argVal);
    }

    return directiveName + '(' + args.join(', ') + ')';
  } else {
    throw new Error('Unsupported type for directive: ' + typeof directiveValue + '. Types allowed: object, boolean.\n' + ('Offending object: ' + JSON.stringify(dirsObj)));
  }
}

function getIndent(level) {
  return Array(level * 4 + 1).join(' ');
}

function filterNonConfigFields(fieldName, ignoreFields) {
  return configFields.indexOf(fieldName) == -1 && ignoreFields.indexOf(fieldName) == -1;
}

function convertQuery(node, level, output, options) {
  Object.keys(node).filter(function (key) {
    return filterNonConfigFields(key, options.ignoreFields);
  }).forEach(function (key) {
    var value = node[key];

    if (typeof value === 'object') {
      if (Array.isArray(value)) {
        value = value.find(function (item) {
          return item && typeof item === 'object';
        });

        if (!value) {
          output.push(['' + key, level]);
          return;
        }
      }

      var fieldCount = Object.keys(value).filter(function (keyCount) {
        return filterNonConfigFields(keyCount, options.ignoreFields);
      }).length;
      var subFields = fieldCount > 0;
      var argsExist = typeof value.__args === 'object';
      var directivesExist = typeof value.__directives === 'object';
      var fullFragmentsExist = value.__all_on instanceof Array;
      var partialFragmentsExist = typeof value.__on === 'object';
      var token = '' + key;

      if (typeof value.__aliasFor === 'string') {
        token = token + ': ' + value.__aliasFor;
      }

      if (typeof value.__variables === 'object') {
        token = token + ' (' + buildVariables(value.__variables) + ')';
      } else if (argsExist || directivesExist) {
        var argsStr = '';
        var dirsStr = '';

        if (directivesExist) {
          var numDirectives = Object.keys(value.__directives).length;

          if (numDirectives > 1) {
            throw new Error('Too many directives. The object/key ' + ("'" + Object.keys(value)[0] + "' had " + numDirectives + ' directives, ') + 'but only 1 directive per object/key is supported at this time.');
          }

          dirsStr = '@' + buildDirectives(value.__directives);
        }

        if (argsExist) {
          argsStr = '(' + buildArgs(value.__args) + ')';
        }

        var spacer = directivesExist && argsExist ? ' ' : '';
        token = token + ' ' + dirsStr + spacer + argsStr;
      }

      output.push([token + (subFields || partialFragmentsExist || fullFragmentsExist ? ' {' : ''), level]);
      convertQuery(value, level + 1, output, options);

      if (fullFragmentsExist) {
        value.__all_on.forEach(function (fullFragment) {
          output.push(['...' + fullFragment, level + 1]);
        });
      }

      if (partialFragmentsExist) {
        var inlineFragments = value.__on instanceof Array ? value.__on : [value.__on];
        inlineFragments.forEach(function (inlineFragment) {
          var name = inlineFragment.__typeName;
          output.push(['... on ' + name + ' {', level + 1]);
          convertQuery(inlineFragment, level + 2, output, options);
          output.push(['}', level + 1]);
        });
      }

      if (subFields || partialFragmentsExist || fullFragmentsExist) {
        output.push(['}', level]);
      }
    } else if (options.includeFalsyKeys === true || value) {
      output.push(['' + key, level]);
    }
  });
}

function convert(query, options) {
  if (options === void 0) {
    options = {};
  }

  if (!query || typeof query != 'object') {
    throw new Error('query object not specified');
  }

  if (Object.keys(query).length == 0) {
    throw new Error('query object has no data');
  }

  if (!(options.ignoreFields instanceof Array)) {
    options.ignoreFields = [];
  }

  var queryLines = [];
  convertQuery(query, 0, queryLines, options);
  var output = '';
  queryLines.forEach(function (_a) {
    var line = _a[0],
        level = _a[1];

    if (options.pretty) {
      if (output) {
        output += '\n';
      }

      output += getIndent(level) + line;
    } else {
      if (output) {
        output += ' ';
      }

      output += line;
    }
  });
  return output;
}



var index$2 = {
  __proto__: null,
  EnumType: EnumType,
  VariableType: VariableType,
  convert: convert,
  configFields: configFields
};

/**
 * <auto-generated>
 *    This code was generated by T4Model template for T4 (https://github.com/linq2db/linq2db).
 *    Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
 * </auto-generated>
 */

/**
 * Уровень конфиденциальности
 */
var confident;

(function (confident) {
  // Обычный
  confident["normal"] = "00000000-0000-0000-0017-000000000001"; // Ограниченный

  confident["restricted"] = "00000000-0000-0000-0017-000000000002"; // Крайне ограниченный

  confident["extremely"] = "00000000-0000-0000-0017-000000000003";
})(confident || (confident = {}));
/**
 * Тип записи структурной единицы
 */


var org_unit_type;

(function (org_unit_type) {
  // Медицинская организация
  org_unit_type["mo"] = "00000000-0000-0000-0022-000000000001"; // Подразделение

  org_unit_type["div"] = "00000000-0000-0000-0022-000000000002"; // Отделение

  org_unit_type["dep"] = "00000000-0000-0000-0022-000000000003"; // Кабинет

  org_unit_type["cab"] = "00000000-0000-0000-0022-000000000004";
})(org_unit_type || (org_unit_type = {}));
/**
 * Вид документа ИС
 */


var doc_type;

(function (doc_type) {
  // Личные документы персоны
  doc_type["privatedoc"] = "00000000-0000-0000-0016-000000000001"; // Личные списки персоны

  doc_type["privatelist"] = "00000000-0000-0000-0016-000000000002"; // Медицинские документы пациента

  doc_type["meddoc"] = "00000000-0000-0000-0016-000000000003"; // Документы сотрудника

  doc_type["staffdoc"] = "00000000-0000-0000-0016-000000000004"; // Документы МО

  doc_type["modoc"] = "00000000-0000-0000-0016-000000000005"; // Документы финансовые

  doc_type["findoc"] = "00000000-0000-0000-0016-000000000006";
})(doc_type || (doc_type = {}));
/**
 * Тип документа организационного
 */


var org_doc_type;

(function (org_doc_type) {
  // Паспорт медицинской организации
  org_doc_type["passport_mo"] = "00000000-0000-0000-0015-000000000001"; // Документ календарного планирования и учета

  org_doc_type["calendar_mo"] = "00000000-0000-0000-0015-000000000002";
})(org_doc_type || (org_doc_type = {}));
/**
 * Внешний источник данных
 */


var source;

(function (source) {})(source || (source = {}));
/**
 * Статус документа
 */


var doc_status;

(function (doc_status) {
  // Не установлен
  doc_status["notset"] = "00000000-0000-0000-0014-000000000001"; // Черновик

  doc_status["draft"] = "00000000-0000-0000-0014-000000000002"; // Рабочий

  doc_status["sign"] = "00000000-0000-0000-0014-000000000003";
})(doc_status || (doc_status = {}));
/**
 * Тип услуги
 */


var service_type;

(function (service_type) {
  // Простая
  service_type["simple"] = "00000000-0000-0000-0011-000000000001";
})(service_type || (service_type = {}));
/**
 * Тип документа финансового ИС
 */


var fin_doc_type;

(function (fin_doc_type) {
  // Прейскурант
  fin_doc_type["pricelist"] = "00000000-0000-0000-0028-000000000001"; // Документ Договор

  fin_doc_type["contract"] = "00000000-0000-0000-0028-000000000002"; // Документ Обращение

  fin_doc_type["recourse"] = "00000000-0000-0000-0028-000000000003"; // Документ Акт об оказанных медицинских услугах

  fin_doc_type["act"] = "00000000-0000-0000-0028-000000000004"; // Документ Оплаты или возврата денежных средств

  fin_doc_type["refund"] = "00000000-0000-0000-0028-000000000005"; // Документ Счет

  fin_doc_type["invoice"] = "00000000-0000-0000-0028-000000000006"; // Лицевой счет

  fin_doc_type["fint"] = "00000000-0000-0000-0028-000000000007";
})(fin_doc_type || (fin_doc_type = {}));
/**
 * Тип направления на обслуживание
 */


var service_direction_type;

(function (service_direction_type) {
  // универсальное направление на обслуживание (формальность)
  service_direction_type["to_direction"] = "00000000-0000-0000-0010-000000000001"; // на прием (осмотр, консультация) врача-специалиста

  service_direction_type["to_see"] = "00000000-0000-0000-0010-000000000002"; // направление на госпитализацию

  service_direction_type["to_hospitalization"] = "00000000-0000-0000-0010-000000000003"; // на лабораторные исследования

  service_direction_type["to_laboratory_research"] = "00000000-0000-0000-0010-000000000004"; // на иные исследования

  service_direction_type["to_other_studies"] = "00000000-0000-0000-0010-000000000005"; // на процедуры и манипуляции

  service_direction_type["to_procedures_manipulations"] = "00000000-0000-0000-0010-000000000006";
})(service_direction_type || (service_direction_type = {}));
/**
 * Источник оплаты медпомощи
 */


var payment_source;

(function (payment_source) {
  // ОМС
  payment_source["oms"] = "00000000-0000-0000-0018-000000000001"; // ДМС

  payment_source["dms"] = "00000000-0000-0000-0018-000000000002"; // Бюджет

  payment_source["budget"] = "00000000-0000-0000-0018-000000000003"; // Средства пациента

  payment_source["patient"] = "00000000-0000-0000-0018-000000000004"; // Третье лицо (юридическое)

  payment_source["third_jur"] = "00000000-0000-0000-0018-000000000005"; // Третье лицо (физическое)

  payment_source["third_phys"] = "00000000-0000-0000-0018-000000000006"; // Иные, разрешенные законодательством РФ

  payment_source["other"] = "00000000-0000-0000-0018-000000000007";
})(payment_source || (payment_source = {}));
/**
 * Тип записи для ценообразования
 */


var pricelist_cost_type;

(function (pricelist_cost_type) {
  // Цена
  pricelist_cost_type["cost"] = "00000000-0000-0000-0027-000000000001"; // Cебестоимость

  pricelist_cost_type["cost_price"] = "00000000-0000-0000-0027-000000000002"; // УЕТ врача

  pricelist_cost_type["UET_doctor"] = "00000000-0000-0000-0027-000000000003"; // УЕТ медсестры

  pricelist_cost_type["UET_nurse"] = "00000000-0000-0000-0027-000000000004"; // УЕТ зубного техника

  pricelist_cost_type["UET_dental_tech"] = "00000000-0000-0000-0027-000000000005";
})(pricelist_cost_type || (pricelist_cost_type = {}));
/**
 * Группа  услуги
 */


var service_group;

(function (service_group) {})(service_group || (service_group = {}));
/**
 * Тип документа по обращению (финансовый)
 */


var fin_recourse_type;

(function (fin_recourse_type) {
  // Талон на оказание услуг
  fin_recourse_type["code1"] = "00000000-0000-0000-0029-000000000001"; // Смета - на оказание услуг (Консолидированный талон на оказание услуг )

  fin_recourse_type["code2"] = "00000000-0000-0000-0029-000000000002"; // Заявление на возврат

  fin_recourse_type["code3"] = "00000000-0000-0000-0029-000000000003"; // Претензия о некачественном оказании услуги

  fin_recourse_type["code4"] = "00000000-0000-0000-0029-000000000004";
})(fin_recourse_type || (fin_recourse_type = {}));
/**
 * Вид документа по обращению (финансовый)
 */


var fin_recourse_kind;

(function (fin_recourse_kind) {
  // Предварительная программа предоставляемых медицинских услуг
  fin_recourse_kind["code2_1"] = "00000000-0000-0000-0030-000000000001"; // Талон на оказание платных услуг

  fin_recourse_kind["code1_1"] = "00000000-0000-0000-0030-000000000002"; // Перечень услуг

  fin_recourse_kind["code2_2"] = "00000000-0000-0000-0030-000000000003";
})(fin_recourse_kind || (fin_recourse_kind = {}));
/**
 * Тип документа Счет
 */


var fin_invoice_type;

(function (fin_invoice_type) {
  // Счет на оплату
  fin_invoice_type["code3101"] = "00000000-0000-0000-0031-000000000001"; // Авансовый счет

  fin_invoice_type["code3102"] = "00000000-0000-0000-0031-000000000002"; // Счет  долговой

  fin_invoice_type["code3103"] = "00000000-0000-0000-0031-000000000003"; // Счет-фактура ( СЧФ )

  fin_invoice_type["code3104"] = "00000000-0000-0000-0031-000000000004";
})(fin_invoice_type || (fin_invoice_type = {}));
/**
 * Тип документа оплаты или возврата денежных средств
 */


var fin_refund_type;

(function (fin_refund_type) {
  // Квитанция об оплате
  fin_refund_type["code3201"] = "00000000-0000-0000-0032-000000000001"; // Приходный ордер

  fin_refund_type["code3202"] = "00000000-0000-0000-0032-000000000002"; // Расходный ордер

  fin_refund_type["code3203"] = "00000000-0000-0000-0032-000000000003"; // Кассовый чек

  fin_refund_type["code3204"] = "00000000-0000-0000-0032-000000000004"; // Электронный чек

  fin_refund_type["code3205"] = "00000000-0000-0000-0032-000000000005";
})(fin_refund_type || (fin_refund_type = {}));
/**
 * Тип транзакции
 */


var fin_trans_type;

(function (fin_trans_type) {
  // Предоплата услуг
  fin_trans_type["code3301"] = "00000000-0000-0000-0033-000000000001"; // Оплата оказанных услуг

  fin_trans_type["code3302"] = "00000000-0000-0000-0033-000000000002"; // Возврат предоплаченных услуг

  fin_trans_type["code3303"] = "00000000-0000-0000-0033-000000000003"; // Возврат суммы с ЛС

  fin_trans_type["code3304"] = "00000000-0000-0000-0033-000000000004"; // Расход: Перевод суммы на другой ЛС

  fin_trans_type["code3305"] = "00000000-0000-0000-0033-000000000005"; // Приход: Перевод суммы с другого ЛС

  fin_trans_type["code3306"] = "00000000-0000-0000-0033-000000000006";
})(fin_trans_type || (fin_trans_type = {}));
/**
 * Тип документа прейскурант
 */


var pricelist_doc_type;

(function (pricelist_doc_type) {
  // ПМУ (на платные медицинские услуги населению)
  pricelist_doc_type["pmu"] = "00000000-0000-0000-0080-000000000001"; // На услуги в системе ДМС

  pricelist_doc_type["dms"] = "00000000-0000-0000-0080-000000000002"; // На услуги в системе ОМС

  pricelist_doc_type["oms"] = "00000000-0000-0000-0080-000000000003"; // На услуги по бюджетным расценкам

  pricelist_doc_type["budget"] = "00000000-0000-0000-0080-000000000004"; // На платные немедицинские услуги

  pricelist_doc_type["paynomed"] = "00000000-0000-0000-0080-000000000005";
})(pricelist_doc_type || (pricelist_doc_type = {}));
/**
 * Способы оплаты
 */


var fin_payment_method;

(function (fin_payment_method) {
  // Наличный расчет - Касса(50)
  fin_payment_method["code3401"] = "00000000-0000-0000-0034-000000000001"; // Безналичный расчет по карте -Банковский перевод(51)

  fin_payment_method["code3402"] = "00000000-0000-0000-0034-000000000002"; // Терминал(57)

  fin_payment_method["code3403"] = "00000000-0000-0000-0034-000000000003";
})(fin_payment_method || (fin_payment_method = {}));
/**
 * Тип документа акт
 */


var fin_act_type;

(function (fin_act_type) {
  // Акт
  fin_act_type["code3501"] = "00000000-0000-0000-0035-000000000001"; // Справка

  fin_act_type["code3502"] = "00000000-0000-0000-0035-000000000002"; // Ведомость оказанных услуг

  fin_act_type["code3503"] = "00000000-0000-0000-0035-000000000003";
})(fin_act_type || (fin_act_type = {}));
/**
 * Статус услуги в финансовом документе
 */


var fin_service_status;

(function (fin_service_status) {
  // включено
  fin_service_status["code3601"] = "00000000-0000-0000-0036-000000000001"; // отменено

  fin_service_status["code3602"] = "00000000-0000-0000-0036-000000000002"; // оформлено заявление на возврат

  fin_service_status["code3603"] = "00000000-0000-0000-0036-000000000003"; // возврат завершен

  fin_service_status["code3604"] = "00000000-0000-0000-0036-000000000004";
})(fin_service_status || (fin_service_status = {}));
/**
 * Тип документа договора
 */


var contract_doc_type;

(function (contract_doc_type) {
  // Основной договор
  contract_doc_type["code3701"] = "00000000-0000-0000-0037-000000000001"; // Доп соглашение о изменении содержания договора

  contract_doc_type["code3702"] = "00000000-0000-0000-0037-000000000002"; // Доп соглашение о расторжении договора

  contract_doc_type["code3703"] = "00000000-0000-0000-0037-000000000003";
})(contract_doc_type || (contract_doc_type = {}));
/**
 * Тип объекта договора
 */


var contract_object_type;

(function (contract_object_type) {
  // на оказание услуг по прейскуранту
  contract_object_type["contract_object_pricelist"] = "00000000-0000-0000-0038-000000000001";
})(contract_object_type || (contract_object_type = {}));
/**
 * Тип документа медицинского в ИС
 */


var med_doc_type;

(function (med_doc_type) {
  // Регистрационные данные МК (титульный лист)
  med_doc_type["medcard"] = "00000000-0000-0000-0021-000000000001"; // Направление на обслуживание

  med_doc_type["servicedir"] = "00000000-0000-0000-0021-000000000002"; // Амбулаторно-поликлиническое обращение

  med_doc_type["amb_case"] = "00000000-0000-0000-0021-000000000003"; // Обращение в стационар

  med_doc_type["hosp_case"] = "00000000-0000-0000-0021-000000000004"; // Посещение

  med_doc_type["visit"] = "00000000-0000-0000-0021-000000000005"; // Диагноз

  med_doc_type["diagnoz"] = "00000000-0000-0000-0021-000000000006"; // Документ специалиста

  med_doc_type["specdoc"] = "00000000-0000-0000-0021-000000000007";
})(med_doc_type || (med_doc_type = {}));
/**
 * Тип контрагента
 */


var contract_contragent_type;

(function (contract_contragent_type) {})(contract_contragent_type || (contract_contragent_type = {}));
/**
 * Тип медицинской карты в ИС
 */


var med_card_type;

(function (med_card_type) {
  // МК амбулаторного пациента
  med_card_type["amb"] = "00000000-0000-0000-0019-000000000001"; // МК стационарного пациента

  med_card_type["stat"] = "00000000-0000-0000-0019-000000000002"; // История родов

  med_card_type["birthhist"] = "00000000-0000-0000-0019-000000000003"; // МК пациента КДЦ

  med_card_type["amb_kdc"] = "00000000-0000-0000-0019-000000000004";
})(med_card_type || (med_card_type = {}));
/**
 * Раздел медицинской карты
 */


var med_card_section;

(function (med_card_section) {})(med_card_section || (med_card_section = {}));
/**
 * Регистрируемое событие (при заведении МК)
 */


var med_card_reason;

(function (med_card_reason) {
  // Открытие новой/ редактирование МК
  med_card_reason["new_card"] = "00000000-0000-0000-0020-000000000001"; // Регистрация направления на обслуживание

  med_card_reason["direction"] = "00000000-0000-0000-0020-000000000002"; // Регистрация оказания услуги

  med_card_reason["service"] = "00000000-0000-0000-0020-000000000003";
})(med_card_reason || (med_card_reason = {}));
/**
 * Житель город/село
 */


var residence_type;

(function (residence_type) {})(residence_type || (residence_type = {}));
/**
 * Тип дополнительных данных персоны в ИС
 */


var person_data_type;

(function (person_data_type) {})(person_data_type || (person_data_type = {}));
/**
 * Группы и категории граждан, имеющие право на получение гос. социальной помощи/льгот
 */


var privilege_category;

(function (privilege_category) {})(privilege_category || (privilege_category = {}));
/**
 * Средство связи
 */


var comm_type;

(function (comm_type) {
  // Домашний телефон
  comm_type["homephone"] = "00000000-0000-0000-0005-000000000001"; // E-mail

  comm_type["email"] = "00000000-0000-0000-0005-000000000002"; // Рабочий телефон

  comm_type["workphone"] = "00000000-0000-0000-0005-000000000003"; // Мобильный телефон

  comm_type["mobilephone"] = "00000000-0000-0000-0005-000000000004";
})(comm_type || (comm_type = {}));
/**
 * Тип  личного документа персоны в ИС
 */


var person_doc_type;

(function (person_doc_type) {
  // Подтвеждающий гражданство
  person_doc_type["citizenship"] = "00000000-0000-0000-0013-000000000001"; // Удостоверяющий личность

  person_doc_type["identity"] = "00000000-0000-0000-0013-000000000002"; // Страховой полис

  person_doc_type["insurance"] = "00000000-0000-0000-0013-000000000003"; // СНИЛС

  person_doc_type["snils"] = "00000000-0000-0000-0013-000000000004"; // Подтверждающий социальную льготу

  person_doc_type["privilege"] = "00000000-0000-0000-0013-000000000005"; // Подтверждающий ивалидность

  person_doc_type["invalid"] = "00000000-0000-0000-0013-000000000006"; // Подтверждающий семейное положение

  person_doc_type["family"] = "00000000-0000-0000-0013-000000000007"; // Подтверждающий социальный статус

  person_doc_type["social"] = "00000000-0000-0000-0013-000000000008"; // Подтверждающий смерть

  person_doc_type["death"] = "00000000-0000-0000-0013-000000000009";
})(person_doc_type || (person_doc_type = {}));
/**
 * Тип адреса персоны
 */


var address_type;

(function (address_type) {
  // Адрес фактического проживания
  address_type["actual"] = "00000000-0000-0000-0004-000000000001"; // Адрес постоянной пописки

  address_type["registration"] = "00000000-0000-0000-0004-000000000002"; // Адрес временной прописки

  address_type["temporary"] = "00000000-0000-0000-0004-000000000003";
})(address_type || (address_type = {}));
/**
 * Пол
 */


var gender;

(function (gender) {
  // мужской
  gender["male"] = "00000000-0000-0000-0006-000000000001"; // женский

  gender["female"] = "00000000-0000-0000-0006-000000000002"; // неизвестно

  gender["unknown"] = "00000000-0000-0000-0006-000000000003";
})(gender || (gender = {}));
/**
 * Реестр страховых медицинских организаций
 */


var smo;

(function (smo) {})(smo || (smo = {}));
/**
 * Вид  личного документа персоны
 */


var person_doc_kind;

(function (person_doc_kind) {})(person_doc_kind || (person_doc_kind = {}));
/**
 * Тип документа сотрудника
 */


var emp_doc_type;

(function (emp_doc_type) {
  // Регистрационные данные
  emp_doc_type["reg"] = "00000000-0000-0000-0008-000000000001"; // Назначение на должность

  emp_doc_type["assign"] = "00000000-0000-0000-0008-000000000002"; // Увольнение

  emp_doc_type["fire"] = "00000000-0000-0000-0008-000000000003";
})(emp_doc_type || (emp_doc_type = {}));
/**
 * Тип картотеки
 */


var emp_file_type;

(function (emp_file_type) {
  // Карточка персонального учета сотрудника
  emp_file_type["emp"] = "00000000-0000-0000-0012-000000000001"; // Карточка персонального учета кандидата на прием

  emp_file_type["candidate"] = "00000000-0000-0000-0012-000000000002";
})(emp_file_type || (emp_file_type = {}));
/**
 * Вид работы
 */


var emp_job_type;

(function (emp_job_type) {
  // Основная
  emp_job_type["main"] = "00000000-0000-0000-0001-000000000001"; // По совместительству

  emp_job_type["combined"] = "00000000-0000-0000-0001-000000000002";
})(emp_job_type || (emp_job_type = {}));
/**
 * Характер работы
 */


var emp_job_format;

(function (emp_job_format) {
  // Постоянная
  emp_job_format["permanent"] = "00000000-0000-0000-0002-000000000001"; // Временная

  emp_job_format["temporary"] = "00000000-0000-0000-0002-000000000002";
})(emp_job_format || (emp_job_format = {}));
/**
 * Вид  назначения
 */


var assign_type;

(function (assign_type) {
  // Принять на работу
  assign_type["assign"] = "00000000-0000-0000-0007-000000000001"; // Перевести временно

  assign_type["temp"] = "00000000-0000-0000-0007-000000000002"; // Перевести постоянно

  assign_type["regular"] = "00000000-0000-0000-0007-000000000003";
})(assign_type || (assign_type = {}));
/**
 * Вид подработки
 */


var emp_side_job_type;

(function (emp_side_job_type) {
  // Внутреннее совместительство
  emp_side_job_type["internal_combination"] = "00000000-0000-0000-0003-000000000001"; // Дополнительная работа

  emp_side_job_type["additional"] = "00000000-0000-0000-0003-000000000002"; // Совмещение

  emp_side_job_type["combination"] = "00000000-0000-0000-0003-000000000003"; // Увеличение объема работ

  emp_side_job_type["increase"] = "00000000-0000-0000-0003-000000000004";
})(emp_side_job_type || (emp_side_job_type = {}));
/**
 * Номенклатура должностей медицинских работников и фармацевтических работников
 */


var post;

(function (post) {})(post || (post = {}));
/**
 * Единица объема мед.помощи (услуги)
 */


var service_unit;

(function (service_unit) {
  // Процедура
  service_unit["procedure"] = "00000000-0000-0000-0081-000000000001"; // Манипуляция

  service_unit["manipulation"] = "00000000-0000-0000-0081-000000000002";
})(service_unit || (service_unit = {}));
/**
 * Тип временного интервала (в часах)
 */


var clock_type;

(function (clock_type) {
  // Дневные (06:00-21:59)
  clock_type["code5501"] = "00000000-0000-0000-0055-000000000001"; // Ночные (22:00-05:59)

  clock_type["code5502"] = "00000000-0000-0000-0055-000000000002"; // Праздничные

  clock_type["code5503"] = "00000000-0000-0000-0055-000000000003"; // Сверхурочные

  clock_type["code5504"] = "00000000-0000-0000-0055-000000000004"; // Основные рабочие

  clock_type["code5505"] = "00000000-0000-0000-0055-000000000005";
})(clock_type || (clock_type = {}));
/**
 * Основание записи дня
 */


var kpu_day_base;

(function (kpu_day_base) {
  // Ручной ввод
  kpu_day_base["code5601"] = "00000000-0000-0000-0056-000000000001"; // День по шаблону графика

  kpu_day_base["code5602"] = "00000000-0000-0000-0056-000000000002"; // Преднастроенный день

  kpu_day_base["code5603"] = "00000000-0000-0000-0056-000000000003";
})(kpu_day_base || (kpu_day_base = {}));
/**
 * Регистр медицинских организаций Российской Федерации
 */


var mo;

(function (mo) {})(mo || (mo = {}));
/**
 * Тип графика
 */


var graphic_type;

(function (graphic_type) {
  // Постоянный
  graphic_type["code5401"] = "00000000-0000-0000-0054-000000000001"; // Скользящий

  graphic_type["code5402"] = "00000000-0000-0000-0054-000000000002";
})(graphic_type || (graphic_type = {}));
/**
 * Тип дня
 */


var day_type;

(function (day_type) {
  // Выходной
  day_type["dayoff"] = "00000000-0000-0000-0009-000000000001"; // Праздничный

  day_type["holiday"] = "00000000-0000-0000-0009-000000000002"; // Рабочий

  day_type["workday"] = "00000000-0000-0000-0009-000000000003"; // Рабочий (предпраздничный)

  day_type["preholiday"] = "00000000-0000-0000-0009-000000000004";
})(day_type || (day_type = {}));
/**
 * Источник Адреса
 */


var address_source;

(function (address_source) {
  // Адрес по ФИАС
  address_source["code4001"] = "00000000-0000-0000-0040-000000000001"; // Адрес строкой (неформализованный)

  address_source["code4002"] = "00000000-0000-0000-0040-000000000002"; // Адрес по показателям (формализованный )

  address_source["code4003"] = "00000000-0000-0000-0040-000000000003"; // Адрес по КЛАДР

  address_source["code4004"] = "00000000-0000-0000-0040-000000000004";
})(address_source || (address_source = {}));
/**
 * Канал направления
 */


var ref_channel;

(function (ref_channel) {})(ref_channel || (ref_channel = {}));
/**
 * Причина статуса направления
 */


var ref_status_reason;

(function (ref_status_reason) {})(ref_status_reason || (ref_status_reason = {}));
/**
 * Тип ЮЛ
 */


var corp_type;

(function (corp_type) {})(corp_type || (corp_type = {}));
/**
 * Тип документа направления на обслуживание
 */


var ref_type;

(function (ref_type) {})(ref_type || (ref_type = {}));
/**
 * Тип неформализованного блока данных
 */


var local_block_type;

(function (local_block_type) {})(local_block_type || (local_block_type = {}));
/**
 * Статус  направления
 */


var ref_status;

(function (ref_status) {})(ref_status || (ref_status = {}));
/**
 * Тип документа КПУ
 */


var kpu_doc_type;

(function (kpu_doc_type) {
  // График работы организации
  kpu_doc_type["code5301"] = "00000000-0000-0000-0053-000000000001"; // График работы сотрудников

  kpu_doc_type["code5302"] = "00000000-0000-0000-0053-000000000002"; // График приема специалистами

  kpu_doc_type["code5303"] = "00000000-0000-0000-0053-000000000003"; // График работы оборудования

  kpu_doc_type["code5304"] = "00000000-0000-0000-0053-000000000004"; // График отпусков

  kpu_doc_type["code5305"] = "00000000-0000-0000-0053-000000000005"; // График дежурств

  kpu_doc_type["code5306"] = "00000000-0000-0000-0053-000000000006"; // График операций

  kpu_doc_type["code5307"] = "00000000-0000-0000-0053-000000000007"; // Учет рабочего времени

  kpu_doc_type["code5308"] = "00000000-0000-0000-0053-000000000008"; // Запись  на медицинское обслуживание

  kpu_doc_type["code5309"] = "00000000-0000-0000-0053-000000000009"; // Запись  на дежурство

  kpu_doc_type["code5310"] = "00000000-0000-0000-0053-000000000010"; // Запись  на операцию

  kpu_doc_type["code5311"] = "00000000-0000-0000-0053-000000000011";
})(kpu_doc_type || (kpu_doc_type = {}));
/**
 * Вид медобслуживания
 */


var med_svc_type;

(function (med_svc_type) {
  // Амбулаторное медицинское обслуживание
  med_svc_type["ambs"] = "00000000-0000-0000-0023-000000000001"; // Стационарное медицинское обслуживание

  med_svc_type["hosp"] = "00000000-0000-0000-0023-000000000002"; // Служба скорой медицинской помощи и дежурная медицинская служба

  med_svc_type["emergency"] = "00000000-0000-0000-0023-000000000003"; // Медицинские услуги по профессиональной трудовой деятельности

  med_svc_type["professional"] = "00000000-0000-0000-0023-000000000004"; // Курортное реабилитационно-восстановительное лечение

  med_svc_type["rehab"] = "00000000-0000-0000-0023-000000000005"; // Предоставление лекарственных препаратов и медицинских средств

  med_svc_type["drug"] = "00000000-0000-0000-0023-000000000006"; // Профилактическое медицинское обслуживание

  med_svc_type["prevention"] = "00000000-0000-0000-0023-000000000007";
})(med_svc_type || (med_svc_type = {}));
/**
 * Тип УК
 */


var exchange_type;

(function (exchange_type) {
  // Обмен файлами
  exchange_type["files"] = "00000000-0000-0000-0050-000000000001";
})(exchange_type || (exchange_type = {}));
/**
 * Тип участник УК
 */


var exchange_member;

(function (exchange_member) {
  // Пользователь личного кабинета, зарегистрированный через КП Эверест-1
  exchange_member["code5101"] = "00000000-0000-0000-0051-000000000001";
})(exchange_member || (exchange_member = {}));
/**
 * Срочность госпитализации
 */


var hosp_urgency;

(function (hosp_urgency) {})(hosp_urgency || (hosp_urgency = {}));
/**
 * Состояние записи УК
 */


var exchange_state;

(function (exchange_state) {
  // Не установлено (ожидает команды)
  exchange_state["code5201"] = "00000000-0000-0000-0052-000000000001"; // Исходящие (поступила команда "Отправить",  но передачи еще не приизошло)

  exchange_state["code5202"] = "00000000-0000-0000-0052-000000000002"; // Выполнено

  exchange_state["code5203"] = "00000000-0000-0000-0052-000000000003";
})(exchange_state || (exchange_state = {}));
/**
 * Состояние обращения
 */


var recourse_condition;

(function (recourse_condition) {})(recourse_condition || (recourse_condition = {}));
/**
 * Вид документа специалиста
 */


var spec_doc_kind;

(function (spec_doc_kind) {})(spec_doc_kind || (spec_doc_kind = {}));
/**
 * Степень обоснованности диагноза
 */


var diag_validity_level;

(function (diag_validity_level) {})(diag_validity_level || (diag_validity_level = {}));
/**
 * Вид нозологической единицы диагноза
 */


var diag_nos_unit;

(function (diag_nos_unit) {})(diag_nos_unit || (diag_nos_unit = {}));
/**
 * Характер заболевания
 */


var disease_nature;

(function (disease_nature) {})(disease_nature || (disease_nature = {}));
/**
 * Метод обоснования диагноза
 */


var diag_valid_method;

(function (diag_valid_method) {})(diag_valid_method || (diag_valid_method = {}));
/**
 * Вид травмы по способу получения
 */


var trauma_receive_type;

(function (trauma_receive_type) {})(trauma_receive_type || (trauma_receive_type = {}));
/**
 * Обстоятельство (цель) посещения
 */


var visit_goal;

(function (visit_goal) {})(visit_goal || (visit_goal = {}));
/**
 * Место оказания медицинской помощи
 */


var assistance_place;

(function (assistance_place) {})(assistance_place || (assistance_place = {}));
/**
 * Дефект госпитального этапа
 */


var hosp_defect;

(function (hosp_defect) {})(hosp_defect || (hosp_defect = {}));
/**
 * Результат обращения
 */


var recourse_result;

(function (recourse_result) {})(recourse_result || (recourse_result = {}));
/**
 * Кодификатор прерванного амбулаторного случая
 */


var recourse_abort_type;

(function (recourse_abort_type) {})(recourse_abort_type || (recourse_abort_type = {}));
/**
 * Тип посещения
 */


var visit_type;

(function (visit_type) {})(visit_type || (visit_type = {}));
/**
 * Порядок случаев госпитализации или обращения
 */


var recourse_order;

(function (recourse_order) {})(recourse_order || (recourse_order = {}));
/**
 * Исход обращения
 */


var recourse_outcome;

(function (recourse_outcome) {})(recourse_outcome || (recourse_outcome = {}));
/**
 * Вид медпомощи
 */


var med_care_type;

(function (med_care_type) {})(med_care_type || (med_care_type = {}));

var dict2 = {
  __proto__: null,
  get confident () { return confident; },
  get org_unit_type () { return org_unit_type; },
  get doc_type () { return doc_type; },
  get org_doc_type () { return org_doc_type; },
  get source () { return source; },
  get doc_status () { return doc_status; },
  get service_type () { return service_type; },
  get fin_doc_type () { return fin_doc_type; },
  get service_direction_type () { return service_direction_type; },
  get payment_source () { return payment_source; },
  get pricelist_cost_type () { return pricelist_cost_type; },
  get service_group () { return service_group; },
  get fin_recourse_type () { return fin_recourse_type; },
  get fin_recourse_kind () { return fin_recourse_kind; },
  get fin_invoice_type () { return fin_invoice_type; },
  get fin_refund_type () { return fin_refund_type; },
  get fin_trans_type () { return fin_trans_type; },
  get pricelist_doc_type () { return pricelist_doc_type; },
  get fin_payment_method () { return fin_payment_method; },
  get fin_act_type () { return fin_act_type; },
  get fin_service_status () { return fin_service_status; },
  get contract_doc_type () { return contract_doc_type; },
  get contract_object_type () { return contract_object_type; },
  get med_doc_type () { return med_doc_type; },
  get contract_contragent_type () { return contract_contragent_type; },
  get med_card_type () { return med_card_type; },
  get med_card_section () { return med_card_section; },
  get med_card_reason () { return med_card_reason; },
  get residence_type () { return residence_type; },
  get person_data_type () { return person_data_type; },
  get privilege_category () { return privilege_category; },
  get comm_type () { return comm_type; },
  get person_doc_type () { return person_doc_type; },
  get address_type () { return address_type; },
  get gender () { return gender; },
  get smo () { return smo; },
  get person_doc_kind () { return person_doc_kind; },
  get emp_doc_type () { return emp_doc_type; },
  get emp_file_type () { return emp_file_type; },
  get emp_job_type () { return emp_job_type; },
  get emp_job_format () { return emp_job_format; },
  get assign_type () { return assign_type; },
  get emp_side_job_type () { return emp_side_job_type; },
  get post () { return post; },
  get service_unit () { return service_unit; },
  get clock_type () { return clock_type; },
  get kpu_day_base () { return kpu_day_base; },
  get mo () { return mo; },
  get graphic_type () { return graphic_type; },
  get day_type () { return day_type; },
  get address_source () { return address_source; },
  get ref_channel () { return ref_channel; },
  get ref_status_reason () { return ref_status_reason; },
  get corp_type () { return corp_type; },
  get ref_type () { return ref_type; },
  get local_block_type () { return local_block_type; },
  get ref_status () { return ref_status; },
  get kpu_doc_type () { return kpu_doc_type; },
  get med_svc_type () { return med_svc_type; },
  get exchange_type () { return exchange_type; },
  get exchange_member () { return exchange_member; },
  get hosp_urgency () { return hosp_urgency; },
  get exchange_state () { return exchange_state; },
  get recourse_condition () { return recourse_condition; },
  get spec_doc_kind () { return spec_doc_kind; },
  get diag_validity_level () { return diag_validity_level; },
  get diag_nos_unit () { return diag_nos_unit; },
  get disease_nature () { return disease_nature; },
  get diag_valid_method () { return diag_valid_method; },
  get trauma_receive_type () { return trauma_receive_type; },
  get visit_goal () { return visit_goal; },
  get assistance_place () { return assistance_place; },
  get hosp_defect () { return hosp_defect; },
  get recourse_result () { return recourse_result; },
  get recourse_abort_type () { return recourse_abort_type; },
  get visit_type () { return visit_type; },
  get recourse_order () { return recourse_order; },
  get recourse_outcome () { return recourse_outcome; },
  get med_care_type () { return med_care_type; }
};

var constants = {
  dict2: dict2
};

var defaultRemoteTypeFuncArgs = function defaultRemoteTypeFuncArgs(ids, remoteTypePkName) {
  var _where;

  return {
    where: (_where = {}, _where[remoteTypePkName] = {
      "in": ids
    }, _where)
  };
};
var dict = {
  schemaName: 'dict2',
  remotes: [{
    remoteTypeName: 'Gender',
    remoteTypePkName: 'genderId'
  }, {
    remoteTypeName: 'PersonDocType',
    remoteTypePkName: 'personDocTypeId'
  }, {
    remoteTypeName: 'PersonDocKind',
    remoteTypePkName: 'personDocKindId',
    remoteTypeFunc: 'findManyPersonDocKind',
    remoteTypeFuncArgs: function remoteTypeFuncArgs(ids) {
      return defaultRemoteTypeFuncArgs(ids, 'personDocKindId');
    }
  }]
};

var person = {
  schemaName: 'person2',
  remotes: [{
    remoteTypeName: 'PersonDoc',
    remoteTypePkName: 'docId'
  }]
};

var index$3 = {
  dict: dict,
  person: person
};

function createCommonjsModule(fn, module) {
	return module = { exports: {} }, fn(module, module.exports), module.exports;
}

var runtime_1 = createCommonjsModule(function (module) {
/**
 * Copyright (c) 2014-present, Facebook, Inc.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

var runtime = (function (exports) {

  var Op = Object.prototype;
  var hasOwn = Op.hasOwnProperty;
  var undefined$1; // More compressible than void 0.
  var $Symbol = typeof Symbol === "function" ? Symbol : {};
  var iteratorSymbol = $Symbol.iterator || "@@iterator";
  var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
  var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";

  function define(obj, key, value) {
    Object.defineProperty(obj, key, {
      value: value,
      enumerable: true,
      configurable: true,
      writable: true
    });
    return obj[key];
  }
  try {
    // IE 8 has a broken Object.defineProperty that only works on DOM objects.
    define({}, "");
  } catch (err) {
    define = function(obj, key, value) {
      return obj[key] = value;
    };
  }

  function wrap(innerFn, outerFn, self, tryLocsList) {
    // If outerFn provided and outerFn.prototype is a Generator, then outerFn.prototype instanceof Generator.
    var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
    var generator = Object.create(protoGenerator.prototype);
    var context = new Context(tryLocsList || []);

    // The ._invoke method unifies the implementations of the .next,
    // .throw, and .return methods.
    generator._invoke = makeInvokeMethod(innerFn, self, context);

    return generator;
  }
  exports.wrap = wrap;

  // Try/catch helper to minimize deoptimizations. Returns a completion
  // record like context.tryEntries[i].completion. This interface could
  // have been (and was previously) designed to take a closure to be
  // invoked without arguments, but in all the cases we care about we
  // already have an existing method we want to call, so there's no need
  // to create a new function object. We can even get away with assuming
  // the method takes exactly one argument, since that happens to be true
  // in every case, so we don't have to touch the arguments object. The
  // only additional allocation required is the completion record, which
  // has a stable shape and so hopefully should be cheap to allocate.
  function tryCatch(fn, obj, arg) {
    try {
      return { type: "normal", arg: fn.call(obj, arg) };
    } catch (err) {
      return { type: "throw", arg: err };
    }
  }

  var GenStateSuspendedStart = "suspendedStart";
  var GenStateSuspendedYield = "suspendedYield";
  var GenStateExecuting = "executing";
  var GenStateCompleted = "completed";

  // Returning this object from the innerFn has the same effect as
  // breaking out of the dispatch switch statement.
  var ContinueSentinel = {};

  // Dummy constructor functions that we use as the .constructor and
  // .constructor.prototype properties for functions that return Generator
  // objects. For full spec compliance, you may wish to configure your
  // minifier not to mangle the names of these two functions.
  function Generator() {}
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}

  // This is a polyfill for %IteratorPrototype% for environments that
  // don't natively support it.
  var IteratorPrototype = {};
  define(IteratorPrototype, iteratorSymbol, function () {
    return this;
  });

  var getProto = Object.getPrototypeOf;
  var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
  if (NativeIteratorPrototype &&
      NativeIteratorPrototype !== Op &&
      hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
    // This environment has a native %IteratorPrototype%; use it instead
    // of the polyfill.
    IteratorPrototype = NativeIteratorPrototype;
  }

  var Gp = GeneratorFunctionPrototype.prototype =
    Generator.prototype = Object.create(IteratorPrototype);
  GeneratorFunction.prototype = GeneratorFunctionPrototype;
  define(Gp, "constructor", GeneratorFunctionPrototype);
  define(GeneratorFunctionPrototype, "constructor", GeneratorFunction);
  GeneratorFunction.displayName = define(
    GeneratorFunctionPrototype,
    toStringTagSymbol,
    "GeneratorFunction"
  );

  // Helper for defining the .next, .throw, and .return methods of the
  // Iterator interface in terms of a single ._invoke method.
  function defineIteratorMethods(prototype) {
    ["next", "throw", "return"].forEach(function(method) {
      define(prototype, method, function(arg) {
        return this._invoke(method, arg);
      });
    });
  }

  exports.isGeneratorFunction = function(genFun) {
    var ctor = typeof genFun === "function" && genFun.constructor;
    return ctor
      ? ctor === GeneratorFunction ||
        // For the native GeneratorFunction constructor, the best we can
        // do is to check its .name property.
        (ctor.displayName || ctor.name) === "GeneratorFunction"
      : false;
  };

  exports.mark = function(genFun) {
    if (Object.setPrototypeOf) {
      Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
    } else {
      genFun.__proto__ = GeneratorFunctionPrototype;
      define(genFun, toStringTagSymbol, "GeneratorFunction");
    }
    genFun.prototype = Object.create(Gp);
    return genFun;
  };

  // Within the body of any async function, `await x` is transformed to
  // `yield regeneratorRuntime.awrap(x)`, so that the runtime can test
  // `hasOwn.call(value, "__await")` to determine if the yielded value is
  // meant to be awaited.
  exports.awrap = function(arg) {
    return { __await: arg };
  };

  function AsyncIterator(generator, PromiseImpl) {
    function invoke(method, arg, resolve, reject) {
      var record = tryCatch(generator[method], generator, arg);
      if (record.type === "throw") {
        reject(record.arg);
      } else {
        var result = record.arg;
        var value = result.value;
        if (value &&
            typeof value === "object" &&
            hasOwn.call(value, "__await")) {
          return PromiseImpl.resolve(value.__await).then(function(value) {
            invoke("next", value, resolve, reject);
          }, function(err) {
            invoke("throw", err, resolve, reject);
          });
        }

        return PromiseImpl.resolve(value).then(function(unwrapped) {
          // When a yielded Promise is resolved, its final value becomes
          // the .value of the Promise<{value,done}> result for the
          // current iteration.
          result.value = unwrapped;
          resolve(result);
        }, function(error) {
          // If a rejected Promise was yielded, throw the rejection back
          // into the async generator function so it can be handled there.
          return invoke("throw", error, resolve, reject);
        });
      }
    }

    var previousPromise;

    function enqueue(method, arg) {
      function callInvokeWithMethodAndArg() {
        return new PromiseImpl(function(resolve, reject) {
          invoke(method, arg, resolve, reject);
        });
      }

      return previousPromise =
        // If enqueue has been called before, then we want to wait until
        // all previous Promises have been resolved before calling invoke,
        // so that results are always delivered in the correct order. If
        // enqueue has not been called before, then it is important to
        // call invoke immediately, without waiting on a callback to fire,
        // so that the async generator function has the opportunity to do
        // any necessary setup in a predictable way. This predictability
        // is why the Promise constructor synchronously invokes its
        // executor callback, and why async functions synchronously
        // execute code before the first await. Since we implement simple
        // async functions in terms of async generators, it is especially
        // important to get this right, even though it requires care.
        previousPromise ? previousPromise.then(
          callInvokeWithMethodAndArg,
          // Avoid propagating failures to Promises returned by later
          // invocations of the iterator.
          callInvokeWithMethodAndArg
        ) : callInvokeWithMethodAndArg();
    }

    // Define the unified helper method that is used to implement .next,
    // .throw, and .return (see defineIteratorMethods).
    this._invoke = enqueue;
  }

  defineIteratorMethods(AsyncIterator.prototype);
  define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
    return this;
  });
  exports.AsyncIterator = AsyncIterator;

  // Note that simple async functions are implemented on top of
  // AsyncIterator objects; they just return a Promise for the value of
  // the final result produced by the iterator.
  exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
    if (PromiseImpl === void 0) PromiseImpl = Promise;

    var iter = new AsyncIterator(
      wrap(innerFn, outerFn, self, tryLocsList),
      PromiseImpl
    );

    return exports.isGeneratorFunction(outerFn)
      ? iter // If outerFn is a generator, return the full iterator.
      : iter.next().then(function(result) {
          return result.done ? result.value : iter.next();
        });
  };

  function makeInvokeMethod(innerFn, self, context) {
    var state = GenStateSuspendedStart;

    return function invoke(method, arg) {
      if (state === GenStateExecuting) {
        throw new Error("Generator is already running");
      }

      if (state === GenStateCompleted) {
        if (method === "throw") {
          throw arg;
        }

        // Be forgiving, per 25.3.3.3.3 of the spec:
        // https://people.mozilla.org/~jorendorff/es6-draft.html#sec-generatorresume
        return doneResult();
      }

      context.method = method;
      context.arg = arg;

      while (true) {
        var delegate = context.delegate;
        if (delegate) {
          var delegateResult = maybeInvokeDelegate(delegate, context);
          if (delegateResult) {
            if (delegateResult === ContinueSentinel) continue;
            return delegateResult;
          }
        }

        if (context.method === "next") {
          // Setting context._sent for legacy support of Babel's
          // function.sent implementation.
          context.sent = context._sent = context.arg;

        } else if (context.method === "throw") {
          if (state === GenStateSuspendedStart) {
            state = GenStateCompleted;
            throw context.arg;
          }

          context.dispatchException(context.arg);

        } else if (context.method === "return") {
          context.abrupt("return", context.arg);
        }

        state = GenStateExecuting;

        var record = tryCatch(innerFn, self, context);
        if (record.type === "normal") {
          // If an exception is thrown from innerFn, we leave state ===
          // GenStateExecuting and loop back for another invocation.
          state = context.done
            ? GenStateCompleted
            : GenStateSuspendedYield;

          if (record.arg === ContinueSentinel) {
            continue;
          }

          return {
            value: record.arg,
            done: context.done
          };

        } else if (record.type === "throw") {
          state = GenStateCompleted;
          // Dispatch the exception by looping back around to the
          // context.dispatchException(context.arg) call above.
          context.method = "throw";
          context.arg = record.arg;
        }
      }
    };
  }

  // Call delegate.iterator[context.method](context.arg) and handle the
  // result, either by returning a { value, done } result from the
  // delegate iterator, or by modifying context.method and context.arg,
  // setting context.delegate to null, and returning the ContinueSentinel.
  function maybeInvokeDelegate(delegate, context) {
    var method = delegate.iterator[context.method];
    if (method === undefined$1) {
      // A .throw or .return when the delegate iterator has no .throw
      // method always terminates the yield* loop.
      context.delegate = null;

      if (context.method === "throw") {
        // Note: ["return"] must be used for ES3 parsing compatibility.
        if (delegate.iterator["return"]) {
          // If the delegate iterator has a return method, give it a
          // chance to clean up.
          context.method = "return";
          context.arg = undefined$1;
          maybeInvokeDelegate(delegate, context);

          if (context.method === "throw") {
            // If maybeInvokeDelegate(context) changed context.method from
            // "return" to "throw", let that override the TypeError below.
            return ContinueSentinel;
          }
        }

        context.method = "throw";
        context.arg = new TypeError(
          "The iterator does not provide a 'throw' method");
      }

      return ContinueSentinel;
    }

    var record = tryCatch(method, delegate.iterator, context.arg);

    if (record.type === "throw") {
      context.method = "throw";
      context.arg = record.arg;
      context.delegate = null;
      return ContinueSentinel;
    }

    var info = record.arg;

    if (! info) {
      context.method = "throw";
      context.arg = new TypeError("iterator result is not an object");
      context.delegate = null;
      return ContinueSentinel;
    }

    if (info.done) {
      // Assign the result of the finished delegate to the temporary
      // variable specified by delegate.resultName (see delegateYield).
      context[delegate.resultName] = info.value;

      // Resume execution at the desired location (see delegateYield).
      context.next = delegate.nextLoc;

      // If context.method was "throw" but the delegate handled the
      // exception, let the outer generator proceed normally. If
      // context.method was "next", forget context.arg since it has been
      // "consumed" by the delegate iterator. If context.method was
      // "return", allow the original .return call to continue in the
      // outer generator.
      if (context.method !== "return") {
        context.method = "next";
        context.arg = undefined$1;
      }

    } else {
      // Re-yield the result returned by the delegate method.
      return info;
    }

    // The delegate iterator is finished, so forget it and continue with
    // the outer generator.
    context.delegate = null;
    return ContinueSentinel;
  }

  // Define Generator.prototype.{next,throw,return} in terms of the
  // unified ._invoke helper method.
  defineIteratorMethods(Gp);

  define(Gp, toStringTagSymbol, "Generator");

  // A Generator should always return itself as the iterator object when the
  // @@iterator function is called on it. Some browsers' implementations of the
  // iterator prototype chain incorrectly implement this, causing the Generator
  // object to not be returned from this call. This ensures that doesn't happen.
  // See https://github.com/facebook/regenerator/issues/274 for more details.
  define(Gp, iteratorSymbol, function() {
    return this;
  });

  define(Gp, "toString", function() {
    return "[object Generator]";
  });

  function pushTryEntry(locs) {
    var entry = { tryLoc: locs[0] };

    if (1 in locs) {
      entry.catchLoc = locs[1];
    }

    if (2 in locs) {
      entry.finallyLoc = locs[2];
      entry.afterLoc = locs[3];
    }

    this.tryEntries.push(entry);
  }

  function resetTryEntry(entry) {
    var record = entry.completion || {};
    record.type = "normal";
    delete record.arg;
    entry.completion = record;
  }

  function Context(tryLocsList) {
    // The root entry object (effectively a try statement without a catch
    // or a finally block) gives us a place to store values thrown from
    // locations where there is no enclosing try statement.
    this.tryEntries = [{ tryLoc: "root" }];
    tryLocsList.forEach(pushTryEntry, this);
    this.reset(true);
  }

  exports.keys = function(object) {
    var keys = [];
    for (var key in object) {
      keys.push(key);
    }
    keys.reverse();

    // Rather than returning an object with a next method, we keep
    // things simple and return the next function itself.
    return function next() {
      while (keys.length) {
        var key = keys.pop();
        if (key in object) {
          next.value = key;
          next.done = false;
          return next;
        }
      }

      // To avoid creating an additional object, we just hang the .value
      // and .done properties off the next function object itself. This
      // also ensures that the minifier will not anonymize the function.
      next.done = true;
      return next;
    };
  };

  function values(iterable) {
    if (iterable) {
      var iteratorMethod = iterable[iteratorSymbol];
      if (iteratorMethod) {
        return iteratorMethod.call(iterable);
      }

      if (typeof iterable.next === "function") {
        return iterable;
      }

      if (!isNaN(iterable.length)) {
        var i = -1, next = function next() {
          while (++i < iterable.length) {
            if (hasOwn.call(iterable, i)) {
              next.value = iterable[i];
              next.done = false;
              return next;
            }
          }

          next.value = undefined$1;
          next.done = true;

          return next;
        };

        return next.next = next;
      }
    }

    // Return an iterator with no values.
    return { next: doneResult };
  }
  exports.values = values;

  function doneResult() {
    return { value: undefined$1, done: true };
  }

  Context.prototype = {
    constructor: Context,

    reset: function(skipTempReset) {
      this.prev = 0;
      this.next = 0;
      // Resetting context._sent for legacy support of Babel's
      // function.sent implementation.
      this.sent = this._sent = undefined$1;
      this.done = false;
      this.delegate = null;

      this.method = "next";
      this.arg = undefined$1;

      this.tryEntries.forEach(resetTryEntry);

      if (!skipTempReset) {
        for (var name in this) {
          // Not sure about the optimal order of these conditions:
          if (name.charAt(0) === "t" &&
              hasOwn.call(this, name) &&
              !isNaN(+name.slice(1))) {
            this[name] = undefined$1;
          }
        }
      }
    },

    stop: function() {
      this.done = true;

      var rootEntry = this.tryEntries[0];
      var rootRecord = rootEntry.completion;
      if (rootRecord.type === "throw") {
        throw rootRecord.arg;
      }

      return this.rval;
    },

    dispatchException: function(exception) {
      if (this.done) {
        throw exception;
      }

      var context = this;
      function handle(loc, caught) {
        record.type = "throw";
        record.arg = exception;
        context.next = loc;

        if (caught) {
          // If the dispatched exception was caught by a catch block,
          // then let that catch block handle the exception normally.
          context.method = "next";
          context.arg = undefined$1;
        }

        return !! caught;
      }

      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        var record = entry.completion;

        if (entry.tryLoc === "root") {
          // Exception thrown outside of any try block that could handle
          // it, so set the completion value of the entire function to
          // throw the exception.
          return handle("end");
        }

        if (entry.tryLoc <= this.prev) {
          var hasCatch = hasOwn.call(entry, "catchLoc");
          var hasFinally = hasOwn.call(entry, "finallyLoc");

          if (hasCatch && hasFinally) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            } else if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else if (hasCatch) {
            if (this.prev < entry.catchLoc) {
              return handle(entry.catchLoc, true);
            }

          } else if (hasFinally) {
            if (this.prev < entry.finallyLoc) {
              return handle(entry.finallyLoc);
            }

          } else {
            throw new Error("try statement without catch or finally");
          }
        }
      }
    },

    abrupt: function(type, arg) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc <= this.prev &&
            hasOwn.call(entry, "finallyLoc") &&
            this.prev < entry.finallyLoc) {
          var finallyEntry = entry;
          break;
        }
      }

      if (finallyEntry &&
          (type === "break" ||
           type === "continue") &&
          finallyEntry.tryLoc <= arg &&
          arg <= finallyEntry.finallyLoc) {
        // Ignore the finally entry if control is not jumping to a
        // location outside the try/catch block.
        finallyEntry = null;
      }

      var record = finallyEntry ? finallyEntry.completion : {};
      record.type = type;
      record.arg = arg;

      if (finallyEntry) {
        this.method = "next";
        this.next = finallyEntry.finallyLoc;
        return ContinueSentinel;
      }

      return this.complete(record);
    },

    complete: function(record, afterLoc) {
      if (record.type === "throw") {
        throw record.arg;
      }

      if (record.type === "break" ||
          record.type === "continue") {
        this.next = record.arg;
      } else if (record.type === "return") {
        this.rval = this.arg = record.arg;
        this.method = "return";
        this.next = "end";
      } else if (record.type === "normal" && afterLoc) {
        this.next = afterLoc;
      }

      return ContinueSentinel;
    },

    finish: function(finallyLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.finallyLoc === finallyLoc) {
          this.complete(entry.completion, entry.afterLoc);
          resetTryEntry(entry);
          return ContinueSentinel;
        }
      }
    },

    "catch": function(tryLoc) {
      for (var i = this.tryEntries.length - 1; i >= 0; --i) {
        var entry = this.tryEntries[i];
        if (entry.tryLoc === tryLoc) {
          var record = entry.completion;
          if (record.type === "throw") {
            var thrown = record.arg;
            resetTryEntry(entry);
          }
          return thrown;
        }
      }

      // The context.catch method must only be called with a location
      // argument that corresponds to a known catch block.
      throw new Error("illegal catch attempt");
    },

    delegateYield: function(iterable, resultName, nextLoc) {
      this.delegate = {
        iterator: values(iterable),
        resultName: resultName,
        nextLoc: nextLoc
      };

      if (this.method === "next") {
        // Deliberately forget the last sent value so that we don't
        // accidentally pass it on to the delegate.
        this.arg = undefined$1;
      }

      return ContinueSentinel;
    }
  };

  // Regardless of whether this script is executing as a CommonJS module
  // or not, return the runtime object so that we can declare the variable
  // regeneratorRuntime in the outer scope, which allows this module to be
  // injected easily by `bin/regenerator --include-runtime script.js`.
  return exports;

}(
  // If this script is executing as a CommonJS module, use module.exports
  // as the regeneratorRuntime namespace. Otherwise create a new empty
  // object. Either way, the resulting object will be used to initialize
  // the regeneratorRuntime variable at the top of this file.
   module.exports 
));

try {
  regeneratorRuntime = runtime;
} catch (accidentalStrictMode) {
  // This module should not be running in strict mode, so the above
  // assignment should always work unless something is misconfigured. Just
  // in case runtime.js accidentally runs in strict mode, in modern engines
  // we can explicitly access globalThis. In older engines we can escape
  // strict mode using a global Function call. This could conceivably fail
  // if a Content Security Policy forbids using Function, but in that case
  // the proper solution is to fix the accidental strict mode problem. If
  // you've misconfigured your bundler to force strict mode and applied a
  // CSP to forbid Function, and you're not willing to fix either of those
  // problems, please detail your unique predicament in a GitHub issue.
  if (typeof globalThis === "object") {
    globalThis.regeneratorRuntime = runtime;
  } else {
    Function("r", "regeneratorRuntime = r")(runtime);
  }
}
});

var Pool = /*#__PURE__*/function () {
  function Pool(options) {
    this.items = [];
    this.free = [];
    this.queue = [];
    this.timeout = 0;
    this.current = 0;
    this.size = 0;
    this.available = 0;
    this.timeout = options.timeout || 0;
  }

  var _proto = Pool.prototype;

  _proto.next = /*#__PURE__*/function () {
    var _next = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee() {
      var _this = this;

      var item, free;
      return runtime_1.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              if (!(this.size === 0)) {
                _context.next = 2;
                break;
              }

              return _context.abrupt("return", null);

            case 2:
              if (!(this.available === 0)) {
                _context.next = 4;
                break;
              }

              return _context.abrupt("return", new Promise(function (resolve, reject) {
                var waiting = {
                  resolve: resolve,
                  timer: null
                };
                waiting.timer = setTimeout(function () {
                  waiting.resolve = null;

                  _this.queue.shift();

                  reject(new Error('Pool next item timeout'));
                }, _this.timeout);

                _this.queue.push(waiting);
              }));

            case 4:
              item = null;
              free = false;

              do {
                item = this.items[this.current];
                free = this.free[this.current];
                this.current++;
                if (this.current === this.size) this.current = 0;
              } while (!item || !free);

              return _context.abrupt("return", item);

            case 8:
            case "end":
              return _context.stop();
          }
        }
      }, _callee, this);
    }));

    function next() {
      return _next.apply(this, arguments);
    }

    return next;
  }();

  _proto.add = function add(item) {
    if (this.items.includes(item)) throw new Error('Pool: add duplicates');
    this.size++;
    this.available++;
    this.items.push(item);
    this.free.push(true);
  };

  _proto.capture = /*#__PURE__*/function () {
    var _capture = /*#__PURE__*/_asyncToGenerator( /*#__PURE__*/runtime_1.mark(function _callee2() {
      var item, index;
      return runtime_1.wrap(function _callee2$(_context2) {
        while (1) {
          switch (_context2.prev = _context2.next) {
            case 0:
              _context2.next = 2;
              return this.next();

            case 2:
              item = _context2.sent;

              if (item) {
                _context2.next = 5;
                break;
              }

              return _context2.abrupt("return", null);

            case 5:
              index = this.items.indexOf(item);
              this.free[index] = false;
              this.available--;
              return _context2.abrupt("return", item);

            case 9:
            case "end":
              return _context2.stop();
          }
        }
      }, _callee2, this);
    }));

    function capture() {
      return _capture.apply(this, arguments);
    }

    return capture;
  }();

  _proto.release = function release(item) {
    var index = this.items.indexOf(item);
    if (index < 0) throw new Error('Pool: release unexpected item');
    if (this.free[index]) throw new Error('Pool: release not captured');
    this.free[index] = true;
    this.available++;

    if (this.queue.length > 0) {
      var _this$queue$shift;

      var _ref = (_this$queue$shift = this.queue.shift()) != null ? _this$queue$shift : {
        resolve: null,
        timer: null
      },
          resolve = _ref.resolve,
          timer = _ref.timer; // as QueueItemType;


      clearTimeout(timer);
      if (resolve) setTimeout(resolve, 0, item);
    }
  };

  return Pool;
}(); // // Usage
// (async () => {
//   const pool = new Pool({ timeout: 5000 });
//   pool.add({ item: 1 });
//   pool.add({ item: 2 });
//   const last = { item: 3 };
//   pool.add(last);
//   const x1 = await pool.capture();
//   console.log({ x1 });
//   const x2 = await pool.capture();
//   console.log({ x2 });
//   const x3 = await pool.capture();
//   console.log({ x3 });
//   pool.capture().then((x4) => {
//     console.log({ x4 });
//   });
//   pool.capture().then((x5) => {
//     console.log({ x5 });
//   });
//   pool.capture().then((x6) => {
//     console.log({ x6 });
//   }).catch((err) => {
//     console.error({ err });
//   });
//   pool.release(x2);
//   pool.release(x1);
//   setTimeout(() => {
//     pool.release(x3);
//   }, 6000);
// })();

//events - a super-basic Javascript (publish subscribe) pattern
var Event = /*#__PURE__*/function () {
  function Event() {
    this.events = {};
  }

  var _proto = Event.prototype;

  _proto.on = function on(eventName, fn) {
    var _this = this;

    this.events[eventName] = this.events[eventName] || [];

    var off = function off() {
      return _this.off(eventName, fn);
    };

    if (!this.events[eventName].includes(fn)) this.events[eventName].push(fn);
    return off;
  };

  _proto.off = function off(eventName, fn) {
    if (this.events[eventName]) {
      for (var i = 0; i < this.events[eventName].length; i++) {
        if (this.events[eventName][i] === fn) {
          this.events[eventName].splice(i, 1);
          break;
        }
      }
    }
  };

  _proto.emmit = function emmit(eventName, data) {
    if (this.events[eventName]) {
      this.events[eventName].forEach(function (fn) {
        fn(data);
      });
    }
  };

  return Event;
}();
var event = /*#__PURE__*/new Event();

var index$4 = {
  Pool: Pool,
  Event: Event,
  event: event
};

exports.classes = index$4;
exports.constants = constants;
exports.json2gql = index$2;
exports.roles = index$1;
exports.subSchema = index$3;
exports.utils = index;
//# sourceMappingURL=share2.cjs.development.js.map
