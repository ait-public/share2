
'use strict'

if (process.env.NODE_ENV === 'production') {
  module.exports = require('./share2.cjs.production.min.js')
} else {
  module.exports = require('./share2.cjs.development.js')
}
