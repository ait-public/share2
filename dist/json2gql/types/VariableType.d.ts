export default class VariableType {
    value: string;
    $VariableType: boolean;
    constructor(value: string);
    toJSON(): string;
}
