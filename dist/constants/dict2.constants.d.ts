/**
 * <auto-generated>
 *    This code was generated by T4Model template for T4 (https://github.com/linq2db/linq2db).
 *    Changes to this file may cause incorrect behavior and will be lost if the code is regenerated.
 * </auto-generated>
 */
/**
 * Уровень конфиденциальности
 */
export declare enum confident {
    normal = "00000000-0000-0000-0017-000000000001",
    restricted = "00000000-0000-0000-0017-000000000002",
    extremely = "00000000-0000-0000-0017-000000000003"
}
/**
 * Тип записи структурной единицы
 */
export declare enum org_unit_type {
    mo = "00000000-0000-0000-0022-000000000001",
    div = "00000000-0000-0000-0022-000000000002",
    dep = "00000000-0000-0000-0022-000000000003",
    cab = "00000000-0000-0000-0022-000000000004"
}
/**
 * Вид документа ИС
 */
export declare enum doc_type {
    privatedoc = "00000000-0000-0000-0016-000000000001",
    privatelist = "00000000-0000-0000-0016-000000000002",
    meddoc = "00000000-0000-0000-0016-000000000003",
    staffdoc = "00000000-0000-0000-0016-000000000004",
    modoc = "00000000-0000-0000-0016-000000000005",
    findoc = "00000000-0000-0000-0016-000000000006"
}
/**
 * Тип документа организационного
 */
export declare enum org_doc_type {
    passport_mo = "00000000-0000-0000-0015-000000000001",
    calendar_mo = "00000000-0000-0000-0015-000000000002"
}
/**
 * Внешний источник данных
 */
export declare enum source {
}
/**
 * Статус документа
 */
export declare enum doc_status {
    notset = "00000000-0000-0000-0014-000000000001",
    draft = "00000000-0000-0000-0014-000000000002",
    sign = "00000000-0000-0000-0014-000000000003"
}
/**
 * Тип услуги
 */
export declare enum service_type {
    simple = "00000000-0000-0000-0011-000000000001"
}
/**
 * Тип документа финансового ИС
 */
export declare enum fin_doc_type {
    pricelist = "00000000-0000-0000-0028-000000000001",
    contract = "00000000-0000-0000-0028-000000000002",
    recourse = "00000000-0000-0000-0028-000000000003",
    act = "00000000-0000-0000-0028-000000000004",
    refund = "00000000-0000-0000-0028-000000000005",
    invoice = "00000000-0000-0000-0028-000000000006",
    fint = "00000000-0000-0000-0028-000000000007"
}
/**
 * Тип направления на обслуживание
 */
export declare enum service_direction_type {
    to_direction = "00000000-0000-0000-0010-000000000001",
    to_see = "00000000-0000-0000-0010-000000000002",
    to_hospitalization = "00000000-0000-0000-0010-000000000003",
    to_laboratory_research = "00000000-0000-0000-0010-000000000004",
    to_other_studies = "00000000-0000-0000-0010-000000000005",
    to_procedures_manipulations = "00000000-0000-0000-0010-000000000006"
}
/**
 * Источник оплаты медпомощи
 */
export declare enum payment_source {
    oms = "00000000-0000-0000-0018-000000000001",
    dms = "00000000-0000-0000-0018-000000000002",
    budget = "00000000-0000-0000-0018-000000000003",
    patient = "00000000-0000-0000-0018-000000000004",
    third_jur = "00000000-0000-0000-0018-000000000005",
    third_phys = "00000000-0000-0000-0018-000000000006",
    other = "00000000-0000-0000-0018-000000000007"
}
/**
 * Тип записи для ценообразования
 */
export declare enum pricelist_cost_type {
    cost = "00000000-0000-0000-0027-000000000001",
    cost_price = "00000000-0000-0000-0027-000000000002",
    UET_doctor = "00000000-0000-0000-0027-000000000003",
    UET_nurse = "00000000-0000-0000-0027-000000000004",
    UET_dental_tech = "00000000-0000-0000-0027-000000000005"
}
/**
 * Группа  услуги
 */
export declare enum service_group {
}
/**
 * Тип документа по обращению (финансовый)
 */
export declare enum fin_recourse_type {
    code1 = "00000000-0000-0000-0029-000000000001",
    code2 = "00000000-0000-0000-0029-000000000002",
    code3 = "00000000-0000-0000-0029-000000000003",
    code4 = "00000000-0000-0000-0029-000000000004"
}
/**
 * Вид документа по обращению (финансовый)
 */
export declare enum fin_recourse_kind {
    code2_1 = "00000000-0000-0000-0030-000000000001",
    code1_1 = "00000000-0000-0000-0030-000000000002",
    code2_2 = "00000000-0000-0000-0030-000000000003"
}
/**
 * Тип документа Счет
 */
export declare enum fin_invoice_type {
    code3101 = "00000000-0000-0000-0031-000000000001",
    code3102 = "00000000-0000-0000-0031-000000000002",
    code3103 = "00000000-0000-0000-0031-000000000003",
    code3104 = "00000000-0000-0000-0031-000000000004"
}
/**
 * Тип документа оплаты или возврата денежных средств
 */
export declare enum fin_refund_type {
    code3201 = "00000000-0000-0000-0032-000000000001",
    code3202 = "00000000-0000-0000-0032-000000000002",
    code3203 = "00000000-0000-0000-0032-000000000003",
    code3204 = "00000000-0000-0000-0032-000000000004",
    code3205 = "00000000-0000-0000-0032-000000000005"
}
/**
 * Тип транзакции
 */
export declare enum fin_trans_type {
    code3301 = "00000000-0000-0000-0033-000000000001",
    code3302 = "00000000-0000-0000-0033-000000000002",
    code3303 = "00000000-0000-0000-0033-000000000003",
    code3304 = "00000000-0000-0000-0033-000000000004",
    code3305 = "00000000-0000-0000-0033-000000000005",
    code3306 = "00000000-0000-0000-0033-000000000006"
}
/**
 * Тип документа прейскурант
 */
export declare enum pricelist_doc_type {
    pmu = "00000000-0000-0000-0080-000000000001",
    dms = "00000000-0000-0000-0080-000000000002",
    oms = "00000000-0000-0000-0080-000000000003",
    budget = "00000000-0000-0000-0080-000000000004",
    paynomed = "00000000-0000-0000-0080-000000000005"
}
/**
 * Способы оплаты
 */
export declare enum fin_payment_method {
    code3401 = "00000000-0000-0000-0034-000000000001",
    code3402 = "00000000-0000-0000-0034-000000000002",
    code3403 = "00000000-0000-0000-0034-000000000003"
}
/**
 * Тип документа акт
 */
export declare enum fin_act_type {
    code3501 = "00000000-0000-0000-0035-000000000001",
    code3502 = "00000000-0000-0000-0035-000000000002",
    code3503 = "00000000-0000-0000-0035-000000000003"
}
/**
 * Статус услуги в финансовом документе
 */
export declare enum fin_service_status {
    code3601 = "00000000-0000-0000-0036-000000000001",
    code3602 = "00000000-0000-0000-0036-000000000002",
    code3603 = "00000000-0000-0000-0036-000000000003",
    code3604 = "00000000-0000-0000-0036-000000000004"
}
/**
 * Тип документа договора
 */
export declare enum contract_doc_type {
    code3701 = "00000000-0000-0000-0037-000000000001",
    code3702 = "00000000-0000-0000-0037-000000000002",
    code3703 = "00000000-0000-0000-0037-000000000003"
}
/**
 * Тип объекта договора
 */
export declare enum contract_object_type {
    contract_object_pricelist = "00000000-0000-0000-0038-000000000001"
}
/**
 * Тип документа медицинского в ИС
 */
export declare enum med_doc_type {
    medcard = "00000000-0000-0000-0021-000000000001",
    servicedir = "00000000-0000-0000-0021-000000000002",
    amb_case = "00000000-0000-0000-0021-000000000003",
    hosp_case = "00000000-0000-0000-0021-000000000004",
    visit = "00000000-0000-0000-0021-000000000005",
    diagnoz = "00000000-0000-0000-0021-000000000006",
    specdoc = "00000000-0000-0000-0021-000000000007"
}
/**
 * Тип контрагента
 */
export declare enum contract_contragent_type {
}
/**
 * Тип медицинской карты в ИС
 */
export declare enum med_card_type {
    amb = "00000000-0000-0000-0019-000000000001",
    stat = "00000000-0000-0000-0019-000000000002",
    birthhist = "00000000-0000-0000-0019-000000000003",
    amb_kdc = "00000000-0000-0000-0019-000000000004"
}
/**
 * Раздел медицинской карты
 */
export declare enum med_card_section {
}
/**
 * Регистрируемое событие (при заведении МК)
 */
export declare enum med_card_reason {
    new_card = "00000000-0000-0000-0020-000000000001",
    direction = "00000000-0000-0000-0020-000000000002",
    service = "00000000-0000-0000-0020-000000000003"
}
/**
 * Житель город/село
 */
export declare enum residence_type {
}
/**
 * Тип дополнительных данных персоны в ИС
 */
export declare enum person_data_type {
}
/**
 * Группы и категории граждан, имеющие право на получение гос. социальной помощи/льгот
 */
export declare enum privilege_category {
}
/**
 * Средство связи
 */
export declare enum comm_type {
    homephone = "00000000-0000-0000-0005-000000000001",
    email = "00000000-0000-0000-0005-000000000002",
    workphone = "00000000-0000-0000-0005-000000000003",
    mobilephone = "00000000-0000-0000-0005-000000000004"
}
/**
 * Тип  личного документа персоны в ИС
 */
export declare enum person_doc_type {
    citizenship = "00000000-0000-0000-0013-000000000001",
    identity = "00000000-0000-0000-0013-000000000002",
    insurance = "00000000-0000-0000-0013-000000000003",
    snils = "00000000-0000-0000-0013-000000000004",
    privilege = "00000000-0000-0000-0013-000000000005",
    invalid = "00000000-0000-0000-0013-000000000006",
    family = "00000000-0000-0000-0013-000000000007",
    social = "00000000-0000-0000-0013-000000000008",
    death = "00000000-0000-0000-0013-000000000009"
}
/**
 * Тип адреса персоны
 */
export declare enum address_type {
    actual = "00000000-0000-0000-0004-000000000001",
    registration = "00000000-0000-0000-0004-000000000002",
    temporary = "00000000-0000-0000-0004-000000000003"
}
/**
 * Пол
 */
export declare enum gender {
    male = "00000000-0000-0000-0006-000000000001",
    female = "00000000-0000-0000-0006-000000000002",
    unknown = "00000000-0000-0000-0006-000000000003"
}
/**
 * Реестр страховых медицинских организаций
 */
export declare enum smo {
}
/**
 * Вид  личного документа персоны
 */
export declare enum person_doc_kind {
}
/**
 * Тип документа сотрудника
 */
export declare enum emp_doc_type {
    reg = "00000000-0000-0000-0008-000000000001",
    assign = "00000000-0000-0000-0008-000000000002",
    fire = "00000000-0000-0000-0008-000000000003"
}
/**
 * Тип картотеки
 */
export declare enum emp_file_type {
    emp = "00000000-0000-0000-0012-000000000001",
    candidate = "00000000-0000-0000-0012-000000000002"
}
/**
 * Вид работы
 */
export declare enum emp_job_type {
    main = "00000000-0000-0000-0001-000000000001",
    combined = "00000000-0000-0000-0001-000000000002"
}
/**
 * Характер работы
 */
export declare enum emp_job_format {
    permanent = "00000000-0000-0000-0002-000000000001",
    temporary = "00000000-0000-0000-0002-000000000002"
}
/**
 * Вид  назначения
 */
export declare enum assign_type {
    assign = "00000000-0000-0000-0007-000000000001",
    temp = "00000000-0000-0000-0007-000000000002",
    regular = "00000000-0000-0000-0007-000000000003"
}
/**
 * Вид подработки
 */
export declare enum emp_side_job_type {
    internal_combination = "00000000-0000-0000-0003-000000000001",
    additional = "00000000-0000-0000-0003-000000000002",
    combination = "00000000-0000-0000-0003-000000000003",
    increase = "00000000-0000-0000-0003-000000000004"
}
/**
 * Номенклатура должностей медицинских работников и фармацевтических работников
 */
export declare enum post {
}
/**
 * Единица объема мед.помощи (услуги)
 */
export declare enum service_unit {
    procedure = "00000000-0000-0000-0081-000000000001",
    manipulation = "00000000-0000-0000-0081-000000000002"
}
/**
 * Тип временного интервала (в часах)
 */
export declare enum clock_type {
    code5501 = "00000000-0000-0000-0055-000000000001",
    code5502 = "00000000-0000-0000-0055-000000000002",
    code5503 = "00000000-0000-0000-0055-000000000003",
    code5504 = "00000000-0000-0000-0055-000000000004",
    code5505 = "00000000-0000-0000-0055-000000000005"
}
/**
 * Основание записи дня
 */
export declare enum kpu_day_base {
    code5601 = "00000000-0000-0000-0056-000000000001",
    code5602 = "00000000-0000-0000-0056-000000000002",
    code5603 = "00000000-0000-0000-0056-000000000003"
}
/**
 * Регистр медицинских организаций Российской Федерации
 */
export declare enum mo {
}
/**
 * Тип графика
 */
export declare enum graphic_type {
    code5401 = "00000000-0000-0000-0054-000000000001",
    code5402 = "00000000-0000-0000-0054-000000000002"
}
/**
 * Тип дня
 */
export declare enum day_type {
    dayoff = "00000000-0000-0000-0009-000000000001",
    holiday = "00000000-0000-0000-0009-000000000002",
    workday = "00000000-0000-0000-0009-000000000003",
    preholiday = "00000000-0000-0000-0009-000000000004"
}
/**
 * Источник Адреса
 */
export declare enum address_source {
    code4001 = "00000000-0000-0000-0040-000000000001",
    code4002 = "00000000-0000-0000-0040-000000000002",
    code4003 = "00000000-0000-0000-0040-000000000003",
    code4004 = "00000000-0000-0000-0040-000000000004"
}
/**
 * Канал направления
 */
export declare enum ref_channel {
}
/**
 * Причина статуса направления
 */
export declare enum ref_status_reason {
}
/**
 * Тип ЮЛ
 */
export declare enum corp_type {
}
/**
 * Тип документа направления на обслуживание
 */
export declare enum ref_type {
}
/**
 * Тип неформализованного блока данных
 */
export declare enum local_block_type {
}
/**
 * Статус  направления
 */
export declare enum ref_status {
}
/**
 * Тип документа КПУ
 */
export declare enum kpu_doc_type {
    code5301 = "00000000-0000-0000-0053-000000000001",
    code5302 = "00000000-0000-0000-0053-000000000002",
    code5303 = "00000000-0000-0000-0053-000000000003",
    code5304 = "00000000-0000-0000-0053-000000000004",
    code5305 = "00000000-0000-0000-0053-000000000005",
    code5306 = "00000000-0000-0000-0053-000000000006",
    code5307 = "00000000-0000-0000-0053-000000000007",
    code5308 = "00000000-0000-0000-0053-000000000008",
    code5309 = "00000000-0000-0000-0053-000000000009",
    code5310 = "00000000-0000-0000-0053-000000000010",
    code5311 = "00000000-0000-0000-0053-000000000011"
}
/**
 * Вид медобслуживания
 */
export declare enum med_svc_type {
    ambs = "00000000-0000-0000-0023-000000000001",
    hosp = "00000000-0000-0000-0023-000000000002",
    emergency = "00000000-0000-0000-0023-000000000003",
    professional = "00000000-0000-0000-0023-000000000004",
    rehab = "00000000-0000-0000-0023-000000000005",
    drug = "00000000-0000-0000-0023-000000000006",
    prevention = "00000000-0000-0000-0023-000000000007"
}
/**
 * Тип УК
 */
export declare enum exchange_type {
    files = "00000000-0000-0000-0050-000000000001"
}
/**
 * Тип участник УК
 */
export declare enum exchange_member {
    code5101 = "00000000-0000-0000-0051-000000000001"
}
/**
 * Срочность госпитализации
 */
export declare enum hosp_urgency {
}
/**
 * Состояние записи УК
 */
export declare enum exchange_state {
    code5201 = "00000000-0000-0000-0052-000000000001",
    code5202 = "00000000-0000-0000-0052-000000000002",
    code5203 = "00000000-0000-0000-0052-000000000003"
}
/**
 * Состояние обращения
 */
export declare enum recourse_condition {
}
/**
 * Вид документа специалиста
 */
export declare enum spec_doc_kind {
}
/**
 * Степень обоснованности диагноза
 */
export declare enum diag_validity_level {
}
/**
 * Вид нозологической единицы диагноза
 */
export declare enum diag_nos_unit {
}
/**
 * Характер заболевания
 */
export declare enum disease_nature {
}
/**
 * Метод обоснования диагноза
 */
export declare enum diag_valid_method {
}
/**
 * Вид травмы по способу получения
 */
export declare enum trauma_receive_type {
}
/**
 * Обстоятельство (цель) посещения
 */
export declare enum visit_goal {
}
/**
 * Место оказания медицинской помощи
 */
export declare enum assistance_place {
}
/**
 * Дефект госпитального этапа
 */
export declare enum hosp_defect {
}
/**
 * Результат обращения
 */
export declare enum recourse_result {
}
/**
 * Кодификатор прерванного амбулаторного случая
 */
export declare enum recourse_abort_type {
}
/**
 * Тип посещения
 */
export declare enum visit_type {
}
/**
 * Порядок случаев госпитализации или обращения
 */
export declare enum recourse_order {
}
/**
 * Исход обращения
 */
export declare enum recourse_outcome {
}
/**
 * Вид медпомощи
 */
export declare enum med_care_type {
}
