export declare type TEventHandler<T> = (data: T) => void;
export declare class Event<T = any> {
    events: Record<string, TEventHandler<T>[]>;
    constructor();
    on(eventName: string, fn: TEventHandler<T>): () => void | null;
    off(eventName: string, fn: TEventHandler<T>): void;
    emmit(eventName: string, data: T): void;
}
declare const _default: Event<any>;
export default _default;
