export declare type QueueItemType = {
    resolve: ((item: any) => void) | null;
    timer: any | null;
};
export declare class Pool {
    items: any[];
    free: boolean[];
    queue: QueueItemType[];
    timeout: number;
    current: number;
    size: number;
    available: number;
    constructor(options: {
        timeout?: number;
    });
    next(): Promise<any>;
    add(item: any): void;
    capture(): Promise<any>;
    release(item: any): void;
}
