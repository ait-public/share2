import { Pool } from './Pool';
import { Event } from './Event';
declare const _default: {
    Pool: typeof Pool;
    Event: typeof Event;
    event: Event<any>;
};
export default _default;
