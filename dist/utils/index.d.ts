export interface IDictionary<T> {
    [Key: string]: T;
}
export declare function flattenArray<T>(arr: any): T[];
export declare function distinct<T>(source: T[]): T[];
export declare function sortByIds(sourceIds: string[], orderedIds: string[]): string[];
export declare function toCamelCase(str: string): string;
export declare function kebab_case(str: string): string;
export declare function hyphenate(str: string): string;
export declare function cloneByJson<T>(obj: T): T;
export declare function traverse(f: (node: any) => any, node: any, prop?: string): any;
/**
 *
 * @param f map function
 * @param nodes array of nodes
 * @param prop property to child nodes
 * @example
 *   utils.traverseAll(
 *    (node: any) => ({ ...node, icon: "mdi-icon", disable: node.id === "2" }),
 *    [{ lev: "1", children: [{ lev: "2", id: "2" }] }]
 *   )
 */
export declare function traverseAll(f: (node: any) => any, nodes?: any[], prop?: string): any;
export declare function isGuid(value: string): boolean;
export declare function uuidv4(): string;
export declare function deepEqual(a: any, b: any, opts?: {
    isStrongVal?: boolean;
    isStrongKey?: boolean;
}): boolean;
export declare function getRandomInt(min: number, max: number): number;
export declare function getRandomString(...args: string[]): string;
export declare function debounce(fn: (...args: any[]) => void, delay: number): (...args: any[]) => void;
export declare function has(object: any, key: string): boolean;
export declare const isObject: (x: any) => boolean;
/**
 * 	Performs a deep merge of objects and returns new object. Does not modify
 * 	objects (immutable) and merges arrays via concatenation.
 *
 * @param objects - Objects to merge
 * @returns {object} New object with merged key/values
 */
export declare function mergeDeep(...objects: IDictionary<any>[]): IDictionary<any>;
/**
 * Регулярное выражение для "год", "года", "лет" ("день", "дня", "дня")
 * @param digit 23
 * @param form1 день (год)
 * @param form2 дня (года)
 * @param form3 дней (лет)
 * @param noDigit show number
 * @returns 23 дня
 */
export declare function getForm(digit: any, form1: string, form2: string, form3: string, noDigit?: boolean): string | null;
export declare enum TrimDateOpts {
    YEAR = 0,
    MONTH = 1,
    DAY = 2,
    HOUR = 3,
    MIN = 4,
    SEC = 5,
    MSEC = 6
}
export declare function trimDate(d: Date | any, opts: TrimDateOpts): Date | undefined;
export interface IPackOpts {
    isEmptyString?: boolean;
    isDefVals?: boolean;
    isEmptyObj?: boolean;
    isEmptyArray?: boolean;
    isNull?: boolean;
    isDistinctArray?: boolean;
    isTrimString?: boolean;
    isZeroDate?: boolean;
    trimDate?: TrimDateOpts;
    removeProps?: string[];
    isParseToDate?: boolean;
}
export declare const NULL_DELPHI_DATE: Date;
export declare function pack(obj: any, removeOpts?: IPackOpts): any | undefined;
export declare function mergePackDeep(items: IDictionary<any>[], opts?: IPackOpts): IDictionary<any>;
export declare function isDate(value: any): boolean;
export declare function formatDate(date: any, fmt?: string): string | undefined;
export declare function formatDateTime(date: any, fmt?: string): string | undefined;
export declare function fio(item?: {
    firstname?: string | null;
    surname?: string | null;
    patronymic?: string | null;
} | null, def?: any): any;
export declare function initialFio(item?: {
    firstname?: string | null;
    surname?: string | null;
    patronymic?: string | null;
} | null, def?: any): any;
export declare function anonymFio(item?: {
    firstname?: string | null;
    surname?: string | null;
    patronymic?: string | null;
} | null, def?: string): any;
export declare function docSerNum(item?: {
    series?: string;
    number?: string;
} | null, def?: string): string;
export declare function ageYears(birthDate: any, otherDate?: any): number;
export declare function ageStr(age: number | string): string | null;
export declare function age(birthDate: any, otherDate?: any): string | null;
/**
 * Replaces "" object prop values with undefined | null
 * @param {any} obj Object
 * @returns cleaned object
 */
export declare function cleanEmptyProps(obj: any, replaceTo?: undefined | null): any;
/**
 * Used in update section of graphql save operations
 * @param chgObj changed object
 * @param origObj original object
 * @param propName name of the object property
 * @returns set clause or undefined
 */
export declare function updateField(chgObj: any, origObj: any, propName: string): {
    set: any;
} | undefined;
export declare function formatBytes(bytes?: number, decimals?: number): string;
export declare function isValidYMD(dateString: string): boolean;
export declare function isValidDMY(dateString: string): boolean;
export declare function getPropValue(o: any, s: string): any;
export declare function search<T>(items: T[], searchQuery?: string, searchProp?: string): T[];
export declare const parseEmail: (str: string) => string;
/**
 * Returns a copy of an object with all keys sorted.
 * @param object target object
 * @param sortWith Its object containing ordered keys or a function to sort the keys (same signature as in Array.prototype.sort()).
 * @returns copy of an object with all keys sorted.
 * @example
 * sortObjectKeys({c:1, b:1, d:1, a:1}, {keys:['b', 'a', 'c']}) // => {b:1, a:1, c:1, d:1}
 * sortObjectKeys({c:1, b:1, d:1, a:1}, {keys:['b', 'a', 'c'], onlyKeys:true}) // => {b:1, a:1, c:1}
   sortObjectKeys({ "key-1": 1, "key-3": 1, "key-10": 1, "key-2": 1},
    {sortFn:(a, b)=>parseInt(a.slice(4))-parseInt(b.slice(4))}
   )
 */
export declare function sortObjectKeys(object?: IDictionary<any>, sortWith?: {
    sortFn?: (a: string, b: string) => number;
    keys?: Array<string>;
    onlyKeys?: boolean;
}): any;
export declare function postOrder(root: any, fn: (obj: any, parent?: any, key?: any) => any | void, forObj?: boolean): any;
export declare function preOrder(root: any, fn: (obj: any, parent?: any, key?: any) => any | void, forObj?: boolean): any;
export declare function escapeHtml(unsafe?: string): string | undefined;
export declare function sleep(ms?: number): Promise<unknown>;
export declare function getOnlyNums(val: string): string;
export declare function validFioRus(val: string): boolean;
/*!
 * Find the differences between two objects and push to a new object
 * (c) 2019 Chris Ferdinandi & Jascha Brinkmann, MIT License, https://gomakethings.com & https://twitter.com/jaschaio
 * @param  {Object} obj1 The original object
 * @param  {Object} obj2 The object to compare against it
 * @return {Object}      An object of differences between the two
 */
export declare function diff(obj1: any, obj2: any): any;
/**
 * @example
 * https://github.com/hughsk/flat
 */
export declare function flatObject(target: any, opts?: {
    delimiter?: string;
    maxDepth?: number;
    transformKey?: any;
    safe?: boolean;
    object?: any;
}): any;
export declare function unflatObject(target: any, opts?: {
    delimiter?: string;
    overwrite?: boolean;
    transformKey?: any;
    object?: any;
}): any;
export declare function capitalizeFirstLetter(str: string): string;
export declare function toSnakeCase(str: string): string;
export declare function getId(entity: Record<string, any>, opts?: {
    defId?: string;
    entityType?: string;
}): any;
