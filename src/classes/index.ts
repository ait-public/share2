import { Pool } from './Pool';
import event, { Event } from './Event';

export default {
  Pool,
  Event,
  event,
};
