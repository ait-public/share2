export type TEventHandler<T> = (data: T) => void;

//events - a super-basic Javascript (publish subscribe) pattern

export class Event<T = any> {
  events: Record<string, TEventHandler<T>[]> = {};

  constructor() {}

  public on(eventName: string, fn: TEventHandler<T>): () => void | null {
    this.events[eventName] = this.events[eventName] || [];
    const off = () => this.off(eventName, fn);
    if (!this.events[eventName].includes(fn)) this.events[eventName].push(fn);
    return off;
  }

  public off(eventName: string, fn: TEventHandler<T>) {
    if (this.events[eventName]) {
      for (var i = 0; i < this.events[eventName].length; i++) {
        if (this.events[eventName][i] === fn) {
          this.events[eventName].splice(i, 1);
          break;
        }
      }
    }
  }

  public emmit(eventName: string, data: T) {
    if (this.events[eventName]) {
      this.events[eventName].forEach(function (fn) {
        fn(data);
      });
    }
  }
}

export default new Event();
