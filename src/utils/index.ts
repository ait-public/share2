import format from 'date-fns/format';
import ru from 'date-fns/locale/ru';

export interface IDictionary<T> {
  [Key: string]: T;
}

export function flattenArray<T>(arr: any): T[] {
  if (!Array.isArray(arr)) {
    return [];
  }
  return arr.reduce((flat: T[], toFlatten: any) => {
    return flat.concat(
      Array.isArray(toFlatten) ? flattenArray(toFlatten) : toFlatten
    );
  }, []);
}

export function distinct<T>(source: T[]) {
  return source.filter(
    (value: T, index: number) => source.indexOf(value) === index
  );
}

export function sortByIds(sourceIds: string[], orderedIds: string[]) {
  if (
    Array.isArray(orderedIds) &&
    orderedIds.length > 0 &&
    Array.isArray(sourceIds) &&
    sourceIds.length > 0
  ) {
    const sortIds = distinct(orderedIds.filter((c) => sourceIds.includes(c)));
    sourceIds = [...sortIds, ...sourceIds.filter((c) => !sortIds.includes(c))];
  }
  return sourceIds;
}

export function toCamelCase(str: string): string {
  return str.replace(/^([A-Z])|\s(\w)/g, (_, p1, p2) => {
    if (p2) {
      return p2.toUpperCase();
    }
    return p1.toLowerCase();
  });
}

export function kebab_case(str: string): string {
  return (str || '').replace(
    /[A-Z]+(?![a-z])|[A-Z]/g,
    ($, ofs) => (ofs ? '-' : '') + $.toLowerCase()
  );
}

// var myStr = 'thisIsAString'; --> "this-is-a-string"
// https://gist.github.com/jonlabelle/5375315
export function hyphenate(str: string): string {
  return str.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase();
}

export function cloneByJson<T>(obj: T): T {
  if (obj === undefined) {
    return obj;
  }
  return JSON.parse(JSON.stringify(obj));
}

export function traverse(
  f: (node: any) => any,
  node: any,
  prop: string = 'children'
): any {
  return f({ ...node, [prop]: traverseAll(f, node[prop], prop) });
}
/**
 *
 * @param f map function
 * @param nodes array of nodes
 * @param prop property to child nodes
 * @example
 *   utils.traverseAll(
 *    (node: any) => ({ ...node, icon: "mdi-icon", disable: node.id === "2" }),
 *    [{ lev: "1", children: [{ lev: "2", id: "2" }] }]
 *   )
 */
export function traverseAll(
  f: (node: any) => any,
  nodes: any[] = [],
  prop: string = 'children'
): any {
  return nodes.map((node) => traverse(f, node, prop));
}

export function isGuid(value: string): boolean {
  if (!value) return false;
  const regex = /[a-f0-9]{8}(?:-[a-f0-9]{4}){3}-[a-f0-9]{12}/i;
  const match = regex.exec(value);
  return match != null;
}

export function uuidv4(): string {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == 'x' ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
}

function toStr(v: any) {
  switch (typeof v) {
    case 'string':
    case 'boolean':
    case 'number':
      return v.toString();
  }

  if (v instanceof Date) return format(v, 'yyyyMMddHHmmss', { locale: ru });

  return null;
}

function isNullBlankOrUndefined(o: any) {
  return typeof o === 'undefined' || o === null || o === '';
}

export function deepEqual(
  a: any,
  b: any,
  opts?: {
    isStrongVal?: boolean;
    isStrongKey?: boolean;
  }
): boolean {
  if (a === b) {
    return true;
  }

  if (!opts?.isStrongVal) {
    if (isNullBlankOrUndefined(a) && isNullBlankOrUndefined(b)) {
      return true;
    }

    const hasDate = a instanceof Date || b instanceof Date;

    if (hasDate) {
      let d1, d2;
      if (a instanceof Date) {
        d1 = toStr(a);
      } else if (isDate(a)) {
        let dt1 = new Date(a);
        if (a.length === 10) dt1 = trimDate(dt1, TrimDateOpts.HOUR) as Date;
        d1 = toStr(dt1);
      }
      if (b instanceof Date) {
        d2 = toStr(b);
      } else if (isDate(b)) {
        let dt2 = new Date(b);
        if (b.length === 10) dt2 = trimDate(dt2, TrimDateOpts.HOUR) as Date;
        d2 = toStr(dt2);
      }
      return d1 === d2;
    }

    const s1 = toStr(a);
    if (s1 !== null) {
      const s2 = toStr(b);
      if (s2 !== null) {
        return s1 === s2;
      }
    }
  }

  if (a instanceof Date && b instanceof Date) {
    // If the values are Date, they were convert to timestamp with getTime and compare it
    if (a.getTime() !== b.getTime()) {
      return false;
    }
  }

  if (a !== Object(a) || b !== Object(b)) {
    // If the values aren't objects, they were already checked for equality
    return false;
  }

  let propsA = Object.keys(a);
  const propsB = Object.keys(b);

  if (!opts?.isStrongKey) {
    if (propsB.length > propsA.length) return false;
    if (propsB.some((p) => !propsA.includes(p))) return false;
    propsA = propsB;
  } else if (propsA.length !== propsB.length) {
    // Different number of props, don't bother to check
    return false;
  }

  return propsA.every((p) => deepEqual(a[p], b[p], opts));
}

export function getRandomInt(min: number, max: number): number {
  return Math.floor(Math.random() * (max - min)) + min;
}

export function getRandomString(...args: string[]): string {
  return args[getRandomInt(0, args.length - 1)];
}

export function debounce(fn: (...args: any[]) => void, delay: number) {
  let timeoutId: any = 0;
  return (...args: any[]) => {
    clearTimeout(timeoutId);
    timeoutId = setTimeout(() => fn(...args), delay);
  };
}

const hasOwnProperty = Object.prototype.hasOwnProperty;

export function has(object: any, key: string) {
  return object != null && hasOwnProperty.call(object, key);
}

// export function throttle2<T extends (...args: any[]) => any>(fn: T, limit: number) {
//     let throttling = false;
//     return (...args: Parameters<T>): void | ReturnType<T> => {
//         if (!throttling) {
//             throttling = true;
//             setTimeout(() => throttling = false, limit);
//             return fn(...args);
//         }
//     };
// }

export const isObject = (x: any) => typeof x === 'object' && x !== null;

/**
 * 	Performs a deep merge of objects and returns new object. Does not modify
 * 	objects (immutable) and merges arrays via concatenation.
 *
 * @param objects - Objects to merge
 * @returns {object} New object with merged key/values
 */
export function mergeDeep(...objects: IDictionary<any>[]): IDictionary<any> {
  const items: any[] = flattenArray(objects).filter((c) => !!c);

  return items
    .filter((c) => !!c)
    .reduce((prev, obj) => {
      Object.keys(obj).forEach((key) => {
        const pVal = prev[key];
        const oVal = obj[key];

        if (Array.isArray(pVal) && Array.isArray(oVal)) {
          prev[key] = distinct(pVal.concat(...oVal));
        } else if (isObject(pVal) && isObject(oVal)) {
          prev[key] = mergeDeep(pVal, oVal);
        } else {
          prev[key] = oVal;
        }
      });

      return prev;
    }, {});
}

/// <summary>
///  Регулярное выражение для "день (год)", "дней (лет)", "дня (года)"
/// </summary>
/// <param name="form1">день</param>
/// <param name="form2">дней</param>
/// <param name="form3">дня</param>

/**
 * Регулярное выражение для "год", "года", "лет" ("день", "дня", "дня")
 * @param digit 23
 * @param form1 день (год)
 * @param form2 дня (года)
 * @param form3 дней (лет)
 * @param noDigit show number
 * @returns 23 дня
 */
export function getForm(
  digit: any,
  form1: string,
  form2: string,
  form3: string,
  noDigit: boolean = false
): string | null {
  digit = parseInt(digit, 10);

  if (isNaN(digit)) {
    return null;
  }

  if (digit < 0) {
    digit = -1 * digit;
  }

  const digitNew = digit % 100;
  let s = noDigit ? '' : digit + ' ';
  const lastFigure = digitNew % 10;

  if (digitNew > 10 && digitNew < 20) {
    s += form3;
  } else if (lastFigure === 1) {
    s += form1;
  } else if (lastFigure > 1 && lastFigure < 5) {
    s += form2;
  } else {
    s += form3;
  }

  return s;
}

export enum TrimDateOpts {
  YEAR = 0,
  MONTH,
  DAY,
  HOUR,
  MIN,
  SEC,
  MSEC,
}

export function trimDate(d: Date | any, opts: TrimDateOpts): Date | undefined {
  if (d instanceof Date) {
    switch (opts) {
      case TrimDateOpts.YEAR:
        return new Date(0, 0, 1);
      case TrimDateOpts.MONTH:
        return new Date(d.getFullYear(), 0, 1);
      case TrimDateOpts.DAY:
        return new Date(d.getFullYear(), d.getMonth(), 1);
      case TrimDateOpts.HOUR:
        return new Date(d.getFullYear(), d.getMonth(), d.getDate(), 0);
      case TrimDateOpts.SEC:
        return new Date(
          d.getFullYear(),
          d.getMonth(),
          d.getDate(),
          d.getHours()
        );
      case TrimDateOpts.MSEC:
        return new Date(
          d.getFullYear(),
          d.getMonth(),
          d.getDate(),
          d.getHours(),
          d.getMilliseconds()
        );
    }
  }

  return undefined;
}

export interface IPackOpts {
  isEmptyString?: boolean;
  isDefVals?: boolean;
  isEmptyObj?: boolean;
  isEmptyArray?: boolean;
  isNull?: boolean;
  isDistinctArray?: boolean;
  isTrimString?: boolean;
  isZeroDate?: boolean;
  trimDate?: TrimDateOpts;
  removeProps?: string[];
  isParseToDate?: boolean;
}

const NULL_DATE = new Date(0);
export const NULL_DELPHI_DATE = new Date('1899-12-30T00:00:00');
const DATE_LEN_1 = '1967-03-31T03:00:00'.length;

export function pack(obj: any, removeOpts?: IPackOpts): any | undefined {
  const opts: IPackOpts = removeOpts || {};

  if (obj === undefined) {
    return undefined;
  }

  if (opts.isNull && obj === null) {
    return undefined;
  }

  if (typeof obj === 'string') {
    if (opts.isTrimString) {
      obj = obj.trim();
    }
    if ((opts.isEmptyString || opts.isDefVals) && obj === '') {
      return undefined;
    }

    if (
      opts?.isParseToDate &&
      obj &&
      obj.length === DATE_LEN_1 &&
      obj[4] === '-' &&
      isDate(obj)
    ) {
      obj = new Date(obj);
    } else {
      return obj;
    }
  }

  if (obj instanceof Date) {
    if (opts.isZeroDate || opts.isDefVals) {
      const t = obj.getTime();
      if (NULL_DATE.getTime() === t || NULL_DELPHI_DATE.getTime() === t) {
        return undefined;
      }
    }

    if (opts.trimDate) {
      return trimDate(obj, opts.trimDate);
    }

    return obj;
  }

  if (Number.isInteger(obj)) {
    if (opts.isDefVals) {
      if (obj === 0 || isNaN(obj)) {
        return undefined;
      }
    }
    return obj;
  }

  if (typeof obj === 'boolean') {
    if (opts.isDefVals && !obj) {
      return undefined;
    }
    return obj;
  }

  if (Array.isArray(obj)) {
    let array = obj.map((c) => pack(c, opts)).filter((c) => c !== undefined);
    if (opts.isEmptyArray && array.length === 0) {
      return undefined;
    }

    if (opts.isDistinctArray) {
      array = distinct(array);
    }

    //

    return array;
  }

  if (typeof obj === 'object') {
    if (obj === null) return null;

    const packObj: IDictionary<any> = {};
    let isEmpty = true;

    for (const prop in obj) {
      if (opts.removeProps && opts.removeProps.includes(prop)) {
        continue;
      }

      if (has(obj, prop)) {
        const val = pack(obj[prop], opts);
        if (val !== undefined) {
          isEmpty = false;
          packObj[prop] = val;
        }
      }
    }

    if (opts.isEmptyObj && isEmpty) {
      return undefined;
    }

    return packObj;
  }

  return undefined;
}

export function mergePackDeep(
  items: IDictionary<any>[],
  opts?: IPackOpts
): IDictionary<any> {
  items = (items || []).filter((c) => !!c).map((c) => pack(c, opts));
  return mergeDeep(items);
}

export function isDate(value: any): boolean {
  switch (typeof value) {
    case 'number':
      return true;
    case 'string':
      return !isNaN(Date.parse(value));
    case 'object':
      if (value instanceof Date) {
        return !isNaN(value.getTime());
      }
      break;
  }
  return false;
}

export function formatDate(
  date: any,
  fmt: string = 'yyyy-MM-dd'
): string | undefined {
  return isDate(date) ? format(new Date(date), fmt, { locale: ru }) : undefined;
}

export function formatDateTime(
  date: any,
  fmt: string = "yyyy-MM-dd'T'HH:mm:ss"
): string | undefined {
  return formatDate(date, fmt);
}

export function fio(
  item?: {
    firstname?: string | null;
    surname?: string | null;
    patronymic?: string | null;
  } | null,
  def: any = null
) {
  if (!item) return def;
  const firstname = item.firstname || '';
  const surname = item.surname || '';
  const patronymic = item.patronymic || '';

  const arr = [surname, firstname, patronymic];
  let sfio = arr.filter((val) => val).join(' ');
  if (sfio) sfio = sfio.trim();
  return sfio || def;
}

export function initialFio(
  item?: {
    firstname?: string | null;
    surname?: string | null;
    patronymic?: string | null;
  } | null,
  def: any = null
) {
  if (!item) return def;
  const firstname = (item.firstname && item.firstname.charAt(0) + '.') || '';
  const patronymic = (item.patronymic && item.patronymic.charAt(0) + '.') || '';
  const surname = item.surname || '';

  let fio = `${surname} ${firstname}${patronymic}`;
  if (fio) fio = fio.trim();
  return fio || def;
}

export function anonymFio(
  item?: {
    firstname?: string | null;
    surname?: string | null;
    patronymic?: string | null;
  } | null,
  def: string = 'аноним'
) {
  return fio(item) || def;
}

export function docSerNum(
  item?: { series?: string; number?: string } | null,
  def = '-'
) {
  if (!item) return def;
  return ((item.series || '') + ' ' + (item.number || '')).trim() || def;
}

export function ageYears(birthDate: any, otherDate?: any) {
  if (!otherDate) otherDate = new Date();
  birthDate = new Date(birthDate);
  otherDate = new Date(otherDate);
  var years = otherDate.getFullYear() - birthDate.getFullYear();
  if (
    otherDate.getMonth() < birthDate.getMonth() ||
    (otherDate.getMonth() === birthDate.getMonth() &&
      otherDate.getDate() < birthDate.getDate())
  ) {
    years--;
  }
  return years;
}

export function ageStr(age: number | string) {
  if (typeof age === 'string') {
    age = parseInt(age, 10);

    if (isNaN(age)) {
      return null;
    }
  }

  let txt;
  let count = age % 100;
  if (count >= 5 && count <= 20) {
    txt = 'лет';
  } else {
    count = count % 10;
    if (count === 1) {
      txt = 'год';
    } else if (count >= 2 && count <= 4) {
      txt = 'года';
    } else {
      txt = 'лет';
    }
  }
  return age + ' ' + txt;
}

export function age(birthDate: any, otherDate?: any) {
  return ageStr(ageYears(birthDate, otherDate));
}

/**
 * Replaces "" object prop values with undefined | null
 * @param {any} obj Object
 * @returns cleaned object
 */
export function cleanEmptyProps(
  obj: any,
  replaceTo: undefined | null = undefined
) {
  if (!obj) return obj;
  return Object.entries(obj).reduce(
    (a: any, [k, v]) => (v !== '' ? ((a[k] = v), a) : ((a[k] = replaceTo), a)),
    {}
  );
}

/**
 * Used in update section of graphql save operations
 * @param chgObj changed object
 * @param origObj original object
 * @param propName name of the object property
 * @returns set clause or undefined
 */
export function updateField(chgObj: any, origObj: any, propName: string) {
  return (
    ((chgObj || {})[propName] !== (origObj || {})[propName] && {
      set: (chgObj || {})[propName] || null,
    }) ||
    undefined
  );
}

export function formatBytes(bytes?: number, decimals = 2) {
  if (!bytes) return '0 Bytes';

  const k = 1024;
  const dm = decimals < 0 ? 0 : decimals;
  const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

  const i = Math.floor(Math.log(bytes) / Math.log(k));

  return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + sizes[i];
}

const RE_YMD = /^\d{4}\-\d{1,2}\-\d{1,2}$/;

export function isValidYMD(dateString: string): boolean {
  // First check for the pattern
  if (!dateString?.length || !RE_YMD.test(dateString)) {
    return false;
  }

  // Parse the date parts to integers
  const parts = dateString.split('-');
  const day = parseInt(parts[2], 10);
  const month = parseInt(parts[1], 10);
  const year = parseInt(parts[0], 10);

  return checkDMY(year, month, day);
}

const RE_DMY = /^\d{1,2}\.\d{1,2}\.\d{4}$/;

export function isValidDMY(dateString: string): boolean {
  // First check for the pattern
  if (!dateString?.length || !RE_DMY.test(dateString)) {
    return false;
  }

  // Parse the date parts to integers
  const parts = dateString.split('.');
  const day = parseInt(parts[0], 10);
  const month = parseInt(parts[1], 10);
  const year = parseInt(parts[2], 10);

  return checkDMY(year, month, day);
}

function checkDMY(year: number, month: number, day: number): boolean {
  // Check the ranges of month and year
  if (year < 1000 || year > 3000 || month === 0 || month > 12) {
    return false;
  }

  const monthLength = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

  // Adjust for leap years
  if (year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0)) {
    monthLength[1] = 29;
  }

  // Check the range of the day
  return day > 0 && day <= monthLength[month - 1];
}

export function getPropValue(o: any, s: string) {
  s = s.replace(/\[(\w+)\]/g, '.$1'); // convert indexes to properties
  s = s.replace(/^\./, ''); // strip a leading dot
  const a = s.split('.');
  for (let i = 0, n = a.length; i < n; ++i) {
    const k = a[i];
    if (k in o) {
      o = o[k];
    } else {
      return;
    }
  }
  return o;
}

export function search<T>(
  items: T[],
  searchQuery?: string,
  searchProp?: string
) {
  if (!searchQuery) return items;
  const searchs = searchQuery.toLowerCase().split(' ');
  return items?.filter((c: any) =>
    searchs.every((v) =>
      ((searchProp ? getPropValue(c, searchProp) : c) + '')
        .toLowerCase()
        .includes(v)
    )
  );
}

const reEmail =
  /(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/g;

export const parseEmail = (str: string) => {
  const t = (str && str.trim().match(reEmail)) || [];
  // if (t) return "mailto:" + t;
  return t.length ? t[0] : '';
};

/**
 * Returns a copy of an object with all keys sorted.
 * @param object target object
 * @param sortWith Its object containing ordered keys or a function to sort the keys (same signature as in Array.prototype.sort()).
 * @returns copy of an object with all keys sorted.
 * @example
 * sortObjectKeys({c:1, b:1, d:1, a:1}, {keys:['b', 'a', 'c']}) // => {b:1, a:1, c:1, d:1}
 * sortObjectKeys({c:1, b:1, d:1, a:1}, {keys:['b', 'a', 'c'], onlyKeys:true}) // => {b:1, a:1, c:1}
   sortObjectKeys({ "key-1": 1, "key-3": 1, "key-10": 1, "key-2": 1},
    {sortFn:(a, b)=>parseInt(a.slice(4))-parseInt(b.slice(4))}
   )
 */
export function sortObjectKeys(
  object?: IDictionary<any>,
  sortWith?: {
    sortFn?: (a: string, b: string) => number;
    keys?: Array<string>;
    onlyKeys?: boolean;
  }
) {
  if (!object) return object;

  let keys = sortWith?.keys || [];
  let sortFn = sortWith?.sortFn;
  let onlyKeys = !!sortWith?.onlyKeys;

  let objectKeys = Object.keys(object);
  return keys.concat(objectKeys.sort(sortFn)).reduce(function (total, key) {
    if (objectKeys.indexOf(key) !== -1) {
      if (!onlyKeys || keys.includes(key)) {
        total[key] = object[key];
      }
    }
    return total;
  }, Object.create(null));
}

function _postOrder(
  root: any,
  parent: any,
  key: any,
  h: { forObj: boolean; fn: (root: any, parent: any, key: any) => any }
): any {
  if (root == null) return;
  if (Array.isArray(root)) {
    const len = root.length;
    for (let i = 0; i < len; i++) {
      const value = root[i];
      if (!h.forObj || (value && typeof value === 'object')) {
        const r = _postOrder(value, root, i, h);
        if (r !== undefined) return r;
      }
    }
  } else if (typeof root === 'object') {
    for (const [key, value] of Object.entries(root)) {
      if (!h.forObj || (value && typeof value === 'object')) {
        const r = _postOrder(value, root, key, h);
        if (r !== undefined) return r;
      }
    }
  }

  return h.fn(root, parent, key);
}

export function postOrder(
  root: any,
  fn: (obj: any, parent?: any, key?: any) => any | void,
  forObj: boolean = true
): any {
  const h = { fn, forObj };

  return _postOrder(root, null, null, h);
}

function _preOrder(
  root: any,
  parent: any,
  key: any,
  h: { forObj: boolean; fn: (root: any, parent: any, key: any) => any }
): any {
  if (root == null) return;
  if (!h.forObj || (root && typeof root === 'object')) {
    const res = h.fn(root, parent, key);
    if (res !== undefined) return res;
  }

  if (Array.isArray(root)) {
    const len = root.length;
    for (let i = 0; i < len; i++) {
      const value = root[i];
      if (!h.forObj || (value && typeof value === 'object')) {
        const r = _preOrder(value, root, i, h);
        if (r !== undefined) return r;
      }
    }
  } else if (typeof root === 'object') {
    for (const [key, value] of Object.entries(root)) {
      if (!h.forObj || (value && typeof value === 'object')) {
        const r = _preOrder(value, root, key, h);
        if (r !== undefined) return r;
      }
    }
  }
}

export function preOrder(
  root: any,
  fn: (obj: any, parent?: any, key?: any) => any | void,
  forObj: boolean = true
): any {
  const h = { fn, forObj };

  return _preOrder(root, null, null, h);
}

export function escapeHtml(unsafe?: string) {
  if (typeof unsafe === 'string') {
    return (
      unsafe &&
      unsafe
        .replace(/&/g, '&amp;')
        .replace(/</g, '&lt;')
        .replace(/>/g, '&gt;')
        .replace(/"/g, '&quot;')
        .replace(/'/g, '&#039;')
    );
  }

  return unsafe;
}

export function sleep(ms?: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function getOnlyNums(val: string) {
  return (val + '').replace(/\D/g, '');
}

const fioRE =
  /^([ ]*[ёа-яЁА-Я][ ]*|[ ]*[ёа-яЁА-Я]+[ёа-яЁА-Я ]*[ёа-яЁА-Я\-ёа-яЁА-Я]?[ёа-яЁА-Я ]*[ёа-яЁА-Я]+[ ]*)$/;

export function validFioRus(val: string) {
  return fioRE.test(val);
}

/*!
 * Find the differences between two objects and push to a new object
 * (c) 2019 Chris Ferdinandi & Jascha Brinkmann, MIT License, https://gomakethings.com & https://twitter.com/jaschaio
 * @param  {Object} obj1 The original object
 * @param  {Object} obj2 The object to compare against it
 * @return {Object}      An object of differences between the two
 */
export function diff(obj1: any, obj2: any) {
  // Make sure an object to compare is provided
  if (!obj2 || Object.prototype.toString.call(obj2) !== '[object Object]') {
    return obj1;
  }

  // if (!obj1 && obj2) return obj2;

  //
  // Variables
  //

  const diffs: any = {};
  let key;

  //
  // Methods
  //

  /**
   * Check if two arrays are equal
   * @param  {Array}   arr1 The first array
   * @param  {Array}   arr2 The second array
   * @return {Boolean}      If true, both arrays are equal
   */
  const arraysMatch = function (arr1: any, arr2: any): boolean {
    // Check if the arrays are the same length
    if (arr1.length !== arr2.length) return false;

    // Check if all items exist and are in the same order
    for (var i = 0; i < arr1.length; i++) {
      if (arr1[i] !== arr2[i]) return false;
    }

    // Otherwise, return true
    return true;
  };

  /**
   * Compare two items and push non-matches to object
   * @param  {*}      item1 The first item
   * @param  {*}      item2 The second item
   * @param  {String} key   The key in our object
   */
  const compare = function (item1: any, item2: any, key: string) {
    // Get the object type
    const type1 = Object.prototype.toString.call(item1);
    const type2 = Object.prototype.toString.call(item2);

    // If type2 is undefined it has been removed
    if (type2 === '[object Undefined]') {
      diffs[key] = null;
      return;
    }

    // If items are different types
    if (type1 !== type2) {
      diffs[key] = item2;
      return;
    }

    // If an object, compare recursively
    if (type1 === '[object Object]') {
      const objDiff = diff(item1, item2);
      if (Object.keys(objDiff).length > 0) {
        diffs[key] = objDiff;
      }
      return;
    }

    // If an array, compare
    if (type1 === '[object Array]') {
      if (!arraysMatch(item1, item2)) {
        diffs[key] = item2;
      }
      return;
    }

    // Else if it's a function, convert to a string and compare
    // Otherwise, just compare
    if (type1 === '[object Function]') {
      if (item1.toString() !== item2.toString()) {
        diffs[key] = item2;
      }
    } else {
      if (item1 !== item2) {
        diffs[key] = item2;
      }
    }
  };

  //
  // Compare our objects
  //

  // Loop through the first object
  for (key in obj1) {
    if (obj1.hasOwnProperty(key)) {
      compare(obj1[key], obj2[key], key);
    }
  }

  // Loop through the second object and find missing items
  for (key in obj2) {
    if (obj2.hasOwnProperty(key)) {
      if (!obj1?.[key] && obj1?.[key] !== obj2[key]) {
        diffs[key] = obj2[key];
      }
    }
  }

  // Return the object of differences
  return diffs;
}

function _isBuffer(obj: any) {
  return (
    obj &&
    obj.constructor &&
    typeof obj.constructor.isBuffer === 'function' &&
    obj.constructor.isBuffer(obj)
  );
}

function _keyIdentity(key: any) {
  return key;
}

// https://github.com/hughsk/flat
/**
 * @example
 * https://github.com/hughsk/flat
 */
export function flatObject(
  target: any,
  opts?: {
    delimiter?: string;
    maxDepth?: number;
    transformKey?: any;
    safe?: boolean;
    object?: any;
  }
) {
  opts = opts || {};

  const delimiter = opts.delimiter || '.';
  const maxDepth = opts.maxDepth || Number.MAX_VALUE;
  const transformKey = opts.transformKey || _keyIdentity;
  const output: any = {};

  function step(object: any, prev?: any, currentDepth?: any) {
    currentDepth = currentDepth || 1;
    Object.keys(object).forEach(function (key) {
      const value = object[key];
      const isarray = opts?.safe && Array.isArray(value);
      const type = Object.prototype.toString.call(value);
      const isbuffer = _isBuffer(value);
      const isobject = type === '[object Object]' || type === '[object Array]';

      const newKey = prev
        ? prev + delimiter + transformKey(key)
        : transformKey(key);

      if (
        !isarray &&
        !isbuffer &&
        isobject &&
        Object.keys(value).length &&
        (!opts?.maxDepth || currentDepth < maxDepth)
      ) {
        return step(value, newKey, currentDepth + 1);
      }

      output[newKey] = value;
    });
  }

  step(target);

  return output;
}

export function unflatObject(
  target: any,
  opts?: {
    delimiter?: string;
    overwrite?: boolean;
    transformKey?: any;
    object?: any;
  }
) {
  opts = opts || {};

  const delimiter = opts.delimiter || '.';
  const overwrite = opts.overwrite || false;
  const transformKey = opts.transformKey || _keyIdentity;
  const result: any = {};

  const isbuffer = _isBuffer(target);
  if (
    isbuffer ||
    Object.prototype.toString.call(target) !== '[object Object]'
  ) {
    return target;
  }

  // safely ensure that the key is
  // an integer.
  function getkey(key: any) {
    const parsedKey = Number(key);

    return isNaN(parsedKey) || key.indexOf('.') !== -1 || opts?.object
      ? key
      : parsedKey;
  }

  function addKeys(keyPrefix: any, recipient: any, target: any) {
    return Object.keys(target).reduce(function (result, key) {
      result[keyPrefix + delimiter + key] = target[key];

      return result;
    }, recipient);
  }

  function isEmpty(val: any): boolean {
    const type = Object.prototype.toString.call(val);
    const isArray = type === '[object Array]';
    const isObject = type === '[object Object]';

    if (!val) {
      return true;
    } else if (isArray) {
      return !val?.length;
    } else if (isObject) {
      return !Object.keys(val)?.length;
    }
    return false;
  }

  target = Object.keys(target).reduce(function (result: any, key) {
    const type = Object.prototype.toString.call(target[key]);
    const isObject = type === '[object Object]' || type === '[object Array]';
    if (!isObject || isEmpty(target[key])) {
      result[key] = target[key];
      return result;
    } else {
      return addKeys(key, result, flatObject(target[key], opts));
    }
  }, {});

  Object.keys(target).forEach(function (key) {
    const split = key.split(delimiter).map(transformKey);
    let key1 = getkey(split.shift());
    let key2 = getkey(split[0]);
    let recipient = result;

    while (key2 !== undefined) {
      if (key1 === '__proto__') {
        return;
      }

      const type = Object.prototype.toString.call(recipient[key1]);
      const isobject = type === '[object Object]' || type === '[object Array]';

      // do not write over falsey, non-undefined values if overwrite is false
      if (!overwrite && !isobject && typeof recipient[key1] !== 'undefined') {
        return;
      }

      if ((overwrite && !isobject) || (!overwrite && recipient[key1] == null)) {
        recipient[key1] = typeof key2 === 'number' && !opts?.object ? [] : {};
      }

      recipient = recipient[key1];
      if (split.length > 0) {
        key1 = getkey(split.shift());
        key2 = getkey(split[0]);
      }
    }

    // unflatten again for 'messy objects'
    recipient[key1] = unflatObject(target[key], opts);
  });

  return result;
}

export function capitalizeFirstLetter(str: string) {
  if (!str?.length) return str;
  return str[0].toUpperCase() + str.slice(1);
}

export function toSnakeCase(str: string) {
  if (!str || !str.length) return str;
  const pattern =
    /[A-Z]{2,}(?=[A-Z][a-z]+[0-9]*|\b)|[A-Z]?[a-z]+[0-9]*|[A-Z]|[0-9]+/g;

  return (
    str
      .match(pattern)
      ?.map((x) => x.toLowerCase())
      .join('_') ?? ''
  );
}

export function getId(
  entity: Record<string, any>,
  opts?: { defId?: string; entityType?: string }
): any {
  if (!entity) return undefined;

  const key = opts?.defId ?? 'id';

  if (entity[key]) return entity[key];

  const Key = capitalizeFirstLetter(key);
  if (entity[Key]) return entity[Key];

  const key_id = toSnakeCase(key);

  if (entity[key_id]) return entity[key_id];

  const entityType = opts?.entityType;
  if (entityType) {
    // service.type | type
    const ktype = entityType.includes('.')
      ? entityType.split('.')[1] ?? ''
      : entityType;

    if (ktype) {
      const keyId = `${ktype}` + Key;
      return getId(entity, { defId: keyId });
    }
  }

  return undefined;
}

// export function flatObject(input: any, separator: string = '.') {
//   function flat(res: {}, key: string, val: { [x: string]: any; }, pre = ''): any {
//     const prefix = [pre, key].filter(v => v).join(separator);
//     return typeof val === 'object'
//       ? Object.keys(val).reduce((prev, curr) => flat(prev, curr, val[curr], prefix), res)
//       : Object.assign(res, { [prefix]: val });
//   }

//   return Object.keys(input).reduce((prev, curr) => flat(prev, curr, input[curr]), {});
// }
