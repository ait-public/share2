export const ANONYM = {
  id: 'ANONYM',
  code: 'anonym',
  code2: 'Аноним',
  name: 'Аноним',
  names: 'Анонимы',
  order: 0,
};

export const USER = {
  id: 'USER',
  code: 'user',
  code2: 'Пользователь',
  name: 'Пользователь',
  names: 'Пользователи',
  order: 5,
};

export const PATIENT = {
  id: 'PATIENT',
  code: 'patient',
  code2: 'Пациент',
  name: 'Пациент',
  names: 'Пациенты',
  order: 10,
};
export const EMPLOYEE = {
  id: 'EMPLOYEE',
  code: 'employee',
  code2: 'Сотрудник',
  name: 'Сотрудник',
  names: 'Сотрудники',
  order: 20,
};
export const ADMIN = {
  id: 'ADMIN',
  code: 'admin',
  code2: 'Админ',
  name: 'Администратор',
  names: 'Администраторы',
  order: 30,
};
export const SYSADMIN = {
  id: 'SYSADMIN',
  code: 'sysadmin',
  code2: 'Сисадмин',
  name: 'Системный администратор',
  names: 'Системные администраторы',
  order: 40,
};
export const SYSTEM = {
  id: 'SYSTEM',
  code: 'system',
  code2: 'Сервис',
  name: 'Системный сервис',
  names: 'Системные сервисы',
  order: 50,
};

export interface IRole {
  id: string;
  code: string;
  code2: string;
  name: string;
  names: string;
  order: number;
}

export const ROLES = new Map<string, IRole>([
  [ANONYM.id, ANONYM],
  [USER.id, USER],
  [PATIENT.id, PATIENT],
  [EMPLOYEE.id, EMPLOYEE],
  [ADMIN.id, ADMIN],
  [SYSADMIN.id, SYSADMIN],
  [SYSTEM.id, SYSTEM],
]);

const _ROLES = Array.from(ROLES.values());

export function findRole(nameOrId: string): IRole {
  if (!nameOrId) return ANONYM;

  nameOrId = nameOrId.toUpperCase();

  if (ROLES.has(nameOrId)) {
    const r = ROLES.get(nameOrId);
    if (r) {
      return r;
    }
  }

  for (const value of _ROLES) {
    if (
      value.name.toUpperCase() === nameOrId ||
      value.names.toUpperCase() === nameOrId
    ) {
      return value;
    }
  }

  return ANONYM;
}

export function findRoles(nameOrIds: string[]): IRole[] {
  return nameOrIds.map((id) => findRole(id));
}

export function finded(r: IRole, q?: string) {
  if (!r) return false;
  if (!q) return true;
  q = q.toUpperCase();

  return r.code.toUpperCase().startsWith(q) ||
    r.name.toUpperCase().startsWith(q) ||
    r.names.toUpperCase().startsWith(q) ||
    r.code2.toUpperCase().startsWith(q)
    ? true
    : false;
}

export function getList({
  ids,
  q,
  where,
}: {
  ids?: string[];
  q?: string;
  where?: { filter?: { roleOrLess?: string } };
}) {
  const roleOrLess = where?.filter?.roleOrLess;
  const flist = ids ? findRoles(ids) : _ROLES;
  const result = flist.filter((c) => finded(c, q));

  if (!roleOrLess) return result;

  const r = findRole(roleOrLess);

  if (!r) return result;

  const rorder = r.order;
  return result.filter((c) => c.order <= rorder);
}

export function isSystem(role: string) {
  return role === SYSTEM.id;
}

export function isSysadmin(role: string) {
  return role === SYSADMIN.id;
}

export function isAdmin(role: string) {
  return role === ADMIN.id;
}
export function isEmployee(role: string) {
  return role === EMPLOYEE.id;
}
export function isPatient(role: string) {
  return role === PATIENT.id;
}

export function isUser(role: string) {
  return role === USER.id;
}

export function isAnonym(role: string) {
  return findRole(role).id === ANONYM.id;
}

export function order(role: string) {
  return findRole(role).order;
}

export function isAdminOrMore(role: string) {
  return order(role) >= ADMIN.order;
}

export function isEmployeeOrMore(role: string) {
  return order(role) >= EMPLOYEE.order;
}

export function isPatientOrMore(role: string) {
  return order(role) >= PATIENT.order;
}

export function isUserOrMore(role: string) {
  return order(role) >= USER.order;
}

export function isAdminOrLess(role: string) {
  return order(role) <= ADMIN.order;
}

export function isEmployeeOrLess(role: string) {
  return order(role) <= EMPLOYEE.order;
}

export function isPatientOrLess(role: string) {
  return order(role) <= PATIENT.order;
}

export function isUserOrLess(role: string) {
  return order(role) <= USER.order;
}
