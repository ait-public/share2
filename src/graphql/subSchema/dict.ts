export const defaultRemoteTypeFuncArgs: (
  ids: any[],
  remoteTypePkName: string
) => object = (ids, remoteTypePkName) => ({
  where: { [remoteTypePkName]: { in: ids } },
});

export default {
  schemaName: 'dict2',
  remotes: [
    {
      remoteTypeName: 'Gender',
      remoteTypePkName: 'genderId',
      // remoteTypeFunc: 'findManyGender', // default
      // remoteTypeFuncArgs: (ids) => gqlTools.defaultRemoteTypeFuncArgs(ids, 'genderId'), // default
    },
    {
      remoteTypeName: 'PersonDocType',
      remoteTypePkName: 'personDocTypeId',
    },
    {
      remoteTypeName: 'PersonDocKind',
      remoteTypePkName: 'personDocKindId',
      remoteTypeFunc: 'findManyPersonDocKind',
      remoteTypeFuncArgs: (ids: any) =>
        defaultRemoteTypeFuncArgs(ids, 'personDocKindId'),
    },
  ],
  // options: { log: true }, // default { log: false, batch: true }
};
