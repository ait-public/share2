export default {
  schemaName: 'person2',
  remotes: [
    {
      remoteTypeName: 'PersonDoc',
      remoteTypePkName: 'docId',
      // remoteTypeFunc: 'findManyGender', // default
      // remoteTypeFuncArgs: (ids) => gqlTools.defaultRemoteTypeFuncArgs(ids, 'genderId'), // default
    },
  ],
  // options: { log: true }, // default { log: false, batch: true }
};
