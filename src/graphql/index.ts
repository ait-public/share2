import dict from './subSchema/dict';
import person from './subSchema/person';

export default {
  dict,
  person,
};
