export default class EnumType {
  value: string;
  $EnumType = true;
  constructor(value: string) {
    this.value = value;
  }
}
