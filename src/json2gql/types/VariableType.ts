export default class VariableType {
  value: string;
  $VariableType = true;
  constructor(value: string) {
    this.value = value;
  }
  toJSON(): string {
    return '$' + this.value;
  }
}
