import EnumType from './types/EnumType';
import VariableType from './types/VariableType';

export const configFields = [
  '__args',
  '__alias',
  '__aliasFor',
  '__variables',
  '__directives',
  '__on',
  '__all_on',
  '__typeName',
];

function stringify(obj_from_json: any): any {
  if (obj_from_json instanceof EnumType || obj_from_json.$EnumType) {
    return obj_from_json.value;
  } else if (
    obj_from_json instanceof VariableType ||
    obj_from_json.$VariableType
  ) {
    return '$' + obj_from_json.value;
  } else if (typeof obj_from_json !== 'object' || obj_from_json === null) {
    return JSON.stringify(obj_from_json);
  } else if (Array.isArray(obj_from_json)) {
    return (
      '[' +
      obj_from_json
        .map(function (item: any) {
          return stringify(item);
        })
        .join(', ') +
      ']'
    );
  }
  const props: any = Object.keys(obj_from_json)
    .map(function (key) {
      return key + ': ' + stringify(obj_from_json[key]);
    })
    .join(', ');
  return '{' + props + '}';
}

function buildArgs(argsObj: any) {
  const args = [];
  for (let argName in argsObj) {
    args.push(argName + ': ' + stringify(argsObj[argName]));
  }
  return args.join(', ');
}

function buildVariables(varsObj: any) {
  const args = [];
  for (let varName in varsObj) {
    args.push('$' + varName + ': ' + varsObj[varName]);
  }
  return args.join(', ');
}

function buildDirectives(dirsObj: any) {
  const directiveName = Object.keys(dirsObj)[0];
  const directiveValue = dirsObj[directiveName];
  if (typeof directiveValue === 'boolean') {
    return directiveName;
  } else if (typeof directiveValue === 'object') {
    const args = [];
    for (let argName in directiveValue) {
      const argVal = stringify(directiveValue[argName]).replace(/"/g, '');
      args.push(argName + ': ' + argVal);
    }
    return directiveName + '(' + args.join(', ') + ')';
  } else {
    throw new Error(
      'Unsupported type for directive: ' +
        typeof directiveValue +
        '. Types allowed: object, boolean.\n' +
        ('Offending object: ' + JSON.stringify(dirsObj))
    );
  }
}

function getIndent(level: number) {
  return Array(level * 4 + 1).join(' ');
}

function filterNonConfigFields(fieldName: string, ignoreFields: string) {
  return (
    configFields.indexOf(fieldName) == -1 &&
    ignoreFields.indexOf(fieldName) == -1
  );
}

function convertQuery(node: any, level: any, output: any, options: any) {
  Object.keys(node)
    .filter(function (key) {
      return filterNonConfigFields(key, options.ignoreFields);
    })
    .forEach(function (key) {
      let value = node[key];
      if (typeof value === 'object') {
        if (Array.isArray(value)) {
          value = value.find(function (item) {
            return item && typeof item === 'object';
          });
          if (!value) {
            output.push(['' + key, level]);
            return;
          }
        }

        const fieldCount = Object.keys(value).filter(function (keyCount) {
          return filterNonConfigFields(keyCount, options.ignoreFields);
        }).length;
        const subFields = fieldCount > 0;
        const argsExist = typeof value.__args === 'object';
        const directivesExist = typeof value.__directives === 'object';
        const fullFragmentsExist = value.__all_on instanceof Array;
        const partialFragmentsExist = typeof value.__on === 'object';
        let token = '' + key;

        if (typeof value.__aliasFor === 'string') {
          token = token + ': ' + value.__aliasFor;
        }
        if (typeof value.__variables === 'object') {
          token = token + ' (' + buildVariables(value.__variables) + ')';
        } else if (argsExist || directivesExist) {
          let argsStr = '';
          let dirsStr = '';
          if (directivesExist) {
            let numDirectives = Object.keys(value.__directives).length;
            if (numDirectives > 1) {
              throw new Error(
                'Too many directives. The object/key ' +
                  ("'" +
                    Object.keys(value)[0] +
                    "' had " +
                    numDirectives +
                    ' directives, ') +
                  'but only 1 directive per object/key is supported at this time.'
              );
            }
            dirsStr = '@' + buildDirectives(value.__directives);
          }
          if (argsExist) {
            argsStr = '(' + buildArgs(value.__args) + ')';
          }
          const spacer = directivesExist && argsExist ? ' ' : '';
          token = token + ' ' + dirsStr + spacer + argsStr;
        }
        output.push([
          token +
            (subFields || partialFragmentsExist || fullFragmentsExist
              ? ' {'
              : ''),
          level,
        ]);
        convertQuery(value, level + 1, output, options);
        if (fullFragmentsExist) {
          value.__all_on.forEach(function (fullFragment: any) {
            output.push(['...' + fullFragment, level + 1]);
          });
        }
        if (partialFragmentsExist) {
          const inlineFragments =
            value.__on instanceof Array ? value.__on : [value.__on];
          inlineFragments.forEach(function (inlineFragment: any) {
            const name = inlineFragment.__typeName;
            output.push(['... on ' + name + ' {', level + 1]);
            convertQuery(inlineFragment, level + 2, output, options);
            output.push(['}', level + 1]);
          });
        }
        if (subFields || partialFragmentsExist || fullFragmentsExist) {
          output.push(['}', level]);
        }
      } else if (options.includeFalsyKeys === true || value) {
        output.push(['' + key, level]);
      }
    });
}

function convert(query: any, options: any) {
  if (options === void 0) {
    options = {};
  }
  if (!query || typeof query != 'object') {
    throw new Error('query object not specified');
  }
  if (Object.keys(query).length == 0) {
    throw new Error('query object has no data');
  }
  if (!(options.ignoreFields instanceof Array)) {
    options.ignoreFields = [];
  }
  const queryLines: any[] = [];
  convertQuery(query, 0, queryLines, options);
  let output = '';
  queryLines.forEach(function (_a: any) {
    const line = _a[0],
      level = _a[1];
    if (options.pretty) {
      if (output) {
        output += '\n';
      }
      output += getIndent(level) + line;
    } else {
      if (output) {
        output += ' ';
      }
      output += line;
    }
  });
  return output;
}

export default convert;
