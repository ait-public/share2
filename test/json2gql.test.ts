import { json2gql } from '../src';
import { pack } from '../src/utils';


describe('json2gql', () => {
    it('Simple Query', () => {

        const query = {
            query: {
                Posts: {
                    id: true,
                    title: true,
                    post_date: true,
                },
            },
        };
        const graphql_query = json2gql.convert(query, { pretty: false });
        expect(graphql_query).toEqual(`query { Posts { id title post_date } }`);
    });

    it('Query with arguments', () => {

        const query = {
            query: {
                Posts: {
                    __args: {
                        where: { id: 2 },
                        orderBy: 'post_date'
                    },
                    id: true,
                    title: true,
                    post_date: true
                }
            }
        };
        const graphql_query = json2gql.convert(query, { pretty: false });
        expect(graphql_query).toEqual(`query { Posts (where: {id: 2}, orderBy: \"post_date\") { id title post_date } }`);
    });

    it('Query with nested objects', () => {

        const query = {
            query: {
                Posts: {
                    id: true,
                    title: true,
                    comments: {
                        id: true,
                        comment: true,
                        user: true,
                    },
                },
            },
        };
        const graphql_query = json2gql.convert(query, { pretty: false });
        expect(graphql_query).toEqual(`query { Posts { id title comments { id comment user } } }`);
    });

    it('Query with Enum Values', () => {

        const query = {
            query: {
                Posts: {
                    __args: {
                        orderBy: 'post_date',
                        status: new json2gql.EnumType('PUBLISHED'),
                    },
                    title: true,
                    body: true,
                },
            },
        };
        const graphql_query = json2gql.convert(query, { pretty: false });
        expect(graphql_query).toEqual(`query { Posts (orderBy: \"post_date\", status: PUBLISHED) { title body } }`);
    });

    it('Query with Enum Values + pack', () => {

        const query = {
            query: {
                Posts: {
                    __args: {
                        orderBy: 'post_date',
                        status: new json2gql.EnumType('PUBLISHED'),
                    },
                    title: true,
                    body: true,
                },
            },
        };
        const graphql_query = json2gql.convert(pack(query), { pretty: false });
        expect(graphql_query).toEqual(`query { Posts (orderBy: \"post_date\", status: PUBLISHED) { title body } }`);
    });

    it('Query with variables', () => {

        const query = {
            query: {
                __variables: {
                    variable1: 'String!',
                    variableWithDefault: 'String = "default_value"',
                },
                Posts: {
                    __args: {
                        arg1: 20,
                        arg2: new json2gql.VariableType('variable1'),
                    },
                    id: true,
                    title: true,
                },
            },
        };

        const graphql_query = json2gql.convert(query, { pretty: false });
        expect(graphql_query).toEqual(`query ($variable1: String!, $variableWithDefault: String = \"default_value\") { Posts (arg1: 20, arg2: $variable1) { id title } }`);
    });


    it('Query with ...', () => {

        const query = {
            query: {
                findMany: {
                    __args: {
                        skip: 0,
                        take: 10,
                        where: { q: 'ss' },
                        orderBy: { id: new json2gql.EnumType("DESC") }
                    },
                    id: true,
                    value: true
                }
            }
        };

        const graphql_query = json2gql.convert(query, { pretty: false });
        expect(graphql_query).toEqual(`query { findMany (skip: 0, take: 10, where: {q: \"ss\"}, orderBy: {id: DESC}) { id value } }`);
    });

});

