import { utils } from '../src';
import { formatDate, isValidDMY, uuidv4, pack, TrimDateOpts, getForm, ageStr, mergePackDeep, deepEqual, search, sortObjectKeys, kebab_case, diff, flatObject, unflatObject, getId } from '../src/utils';



describe('postOrder', () => {
  it('postOrder', () => {


    const o = {
      "__typename": "XqUserProfilePayload",
      "record": {
        "id": "MTUw",
        "roleType": {
          "__typename": "XqRoleTypePayload",
          "record": {
            "id": "MA==",
            "value": "Cотрудник"
          }
        },
        "armType": {
          "__typename": "XqARMTypePayload",
          "record": {
            "id": "MTg=",
            "value": "АРМ регистратуры поликлиники (КДО)"
          }
        },
        "empAssign": {
          "record": {
            "id": "RyAgIDE0OQ=="
          }
        },
        "begDate": "2014-01-14T00:00:00"
      }
    };

    const r = utils.postOrder(o, (obj) => {
      if (obj?.id === "RyAgIDE0OQ==") {
        return obj
      }
    });
    expect(r).toBe(o.record.empAssign.record);


    const r_ = utils.preOrder(o, (obj) => {
      if (obj?.id === "RyAgIDE0OQ==") {
        return obj
      }
    });
    expect(r_).toBe(o.record.empAssign.record);

    const o1 = {
      "p0": 0,
      "p1": {
        "p2": 2,
        "p3": {
          "p4": 4,
          "p5": {
            "p6": 6,
            "p7": 7
          }
        },
        "p8": [9, 10, 11, { "p12": { "p13": 13 } }],
        "p14": {
          "p15": 15
        },
        "p16": 16
      }
    };

    const r1: any = [];
    utils.postOrder(o1, (_, __, key) => {
      r1.push(key)
    }, true);

    expect(r1).toStrictEqual(['p5', 'p3', 'p12', 3, 'p8', 'p14', 'p1', null]);

    const r2: any = [];
    utils.postOrder(o1, (_, __, key) => {
      r2.push(key)
    }, false);

    expect(r2).toStrictEqual(
      ["p0", "p2", "p4", "p6", "p7", "p5", "p3", 0, 1, 2, "p13", "p12", 3, "p8", "p15", "p14", "p16", "p1", null,])


    const r3: any = [];
    utils.preOrder(o1, (_, __, key) => {
      r3.push(key)
    });

    expect(r3).toStrictEqual(
      [
        null, 'p1', 'p3',
        'p5', 'p8', 3,
        'p12', 'p14'
      ])

  });
});




describe('traverseAll', () => {
  it('works', () => {

    const tree = utils.traverseAll(
      (node) => ({ ...node, icon: 'mdi-icon', disable: node.id === '2' }),
      [{ lev: '1', children: [{ lev: '2', id: '2' }] }]
    )

    expect(tree).toMatchObject([
      {
        lev: '1',
        children: [{
          lev: '2',
          id: '2',
          disable: true,
          icon: 'mdi-icon',
          children: []
        }],
        icon: 'mdi-icon',
        disable: false
      }
    ]);
  });
});

describe('fio', () => {
  it('works', () => {
    const obj = {
      firstname: 'Иван',
      patronymic: 'Петрович',
      surname: 'Сидоров',
    };
    const { patronymic, ...rest } = obj;

    let fio = utils.fio(obj);
    expect(fio).toEqual('Сидоров Иван Петрович');

    fio = utils.fio(rest);
    expect(fio).toEqual('Сидоров Иван');

    fio = utils.initialFio(obj);
    expect(fio).toEqual('Сидоров И.П.');

    fio = utils.initialFio(rest);
    expect(fio).toEqual('Сидоров И.');
  });
});


describe('formatDate', () => {
  it('works', () => {
    const d = new Date(2020, 3, 12);
    expect(formatDate(d)).toEqual('2020-04-12');
    expect(formatDate(new Date('2020-03-21'))).toEqual('2020-03-21');
    expect(formatDate('2020-03-21T01:12')).toEqual('2020-03-21');
    expect(formatDate('2021-03-02T11:23:06.000Z')).toEqual('2021-03-02');
    expect(formatDate('Mon May 05 1947 03:00:00 GMT+0300 (Москва, стандартное время)', 'dd.MM.yyyy')).toEqual('05.05.1947');
  });
});


describe('isValidDMY', () => {
  it('works', () => {
    expect(isValidDMY('32.01.2001')).toBeFalsy();
    expect(isValidDMY('31.01-2001')).toBeFalsy();
    expect(isValidDMY('31.01.2001')).toBeTruthy();
    expect(isValidDMY('00.01.2001')).toBeFalsy();
  });
});


describe('uuidv4', () => {
  it('works', () => {
    // console.log(uuidv4());
    expect(uuidv4()).toBeDefined();
  });
});

describe('getForm', () => {
  it('works', () => {
    expect(getForm(0, "штука", "штуки", "штук")).toBe("0 штук")
    expect(getForm(3, "штука", "штуки", "штук")).toBe("3 штуки")
    expect(getForm(1, "год", "года", "лет")).toBe("1 год")
    expect(getForm(2, "год", "года", "лет")).toBe("2 года")
    expect(getForm(5, "год", "года", "лет")).toBe("5 лет")
    expect(getForm(23, "день", "дня", "дней")).toBe("23 дня")
    expect(getForm(125, "день", "дня", "дней")).toBe("125 дней")
    expect(getForm(65, "день", "дня", "дней", true)).toBe("дней")
  });
});

describe('pack', () => {
  it('works', () => {
    const p1 = pack({ s: "", b: false, i: 0, u: undefined, null: null });
    expect(p1).toEqual({ s: "", b: false, i: 0, null: null });

    const p2 = pack({ s: "", b: false, i: 0, u: undefined, null: null }, { isNull: true });
    expect(p2).toEqual({ s: "", b: false, i: 0 });

    const p3 = pack({ s: "", b: false, i: 0, u: undefined, null: null }, { isNull: true, isEmptyString: true });
    expect(p3).toEqual({ b: false, i: 0 });

    const p4 = pack({ s: "  ", b: false, i: 0, u: undefined, null: null }, { isNull: true, isTrimString: true, isEmptyString: true });
    expect(p4).toEqual({ b: false, i: 0 });

    const p5 = pack({ s: "  ", b: false, i: 0, u: undefined, null: null }, { isNull: true, isTrimString: true });
    expect(p5).toEqual({ s: "", b: false, i: 0 });

    const p6 = pack({ s: " gg ", b: false, i: 0, u: undefined, null: null }, { isNull: true, isTrimString: true });
    expect(p6).toEqual({ s: "gg", b: false, i: 0 });

    const p7 = pack({ s: "", b: false, i: 0, u: undefined, null: null }, { isNull: true, isDefVals: true });
    expect(p7).toEqual({});

    const p8 = pack({ s: "", b: {} }, { isNull: true, isEmptyObj: true, isDefVals: true });
    expect(p8).toEqual(undefined);

    const p9 = pack({ a: [] }, { isEmptyArray: true });
    expect(p9).toEqual({});

    const p10 = pack({ a: ["2", "2", "1"] }, { isDistinctArray: true });
    expect(p10).toEqual({ a: ["2", "1"] });

    const p11 = pack({ t: [], a: { k: ["2", "2", "1"], t: { f: 0, s: "", r: {} } }, }, {
      isDistinctArray: true, isDefVals: true, isEmptyObj: true, isEmptyArray: true
    });
    expect(p11).toEqual({ a: { k: ["2", "1"] } });

    const today = new Date();
    const p12 = pack({ t: today }, { trimDate: TrimDateOpts.HOUR });
    expect(p12).toEqual({ t: new Date(today.getFullYear(), today.getMonth(), today.getDate()) });

    const p13 = pack({ t: { d: "", g: { d: { hh: "###" } }, kk: "fff" }, kk: [] }, { removeProps: ["d", "kk"] });
    expect(p13).toEqual({ t: { g: {} } });

    const p14 = pack({ t: { d: "", g: { d: { hh: "", l: {}, b: false } }, kk: "" }, kk: [], h: { i: 0 } },
      { isDefVals: true, isEmptyArray: true, isEmptyObj: true });
    expect(p14).toEqual(undefined);

    const p15 = pack({ birthDate: '1967-03-31T03:00:00', issueDate: '2019-0410T03:00:00' }, { isParseToDate: true });
    expect(p15).toMatchObject({ birthDate: new Date('1967-03-31T03:00:00'), issueDate: '2019-0410T03:00:00' });

    const p16 = pack("1899-12-30T00:00:00", { isZeroDate: true, isParseToDate: true });
    expect(p16).toEqual(undefined);

  });
});

describe('ageStr', () => {
  it('works', () => {
    expect(ageStr("1")).toEqual('1 год');
    expect(ageStr(2)).toEqual('2 года');
  });
});

describe('mergePackDeep', () => {
  it('works', () => {
    expect(mergePackDeep([{ k: { f: "ee", j: [] } }, { s: undefined, f: 120 }])).toEqual({ "f": 120, "k": { "f": "ee", "j": [] } });
  });
});

describe('deepEqual', () => {
  it('works', () => {
    expect(deepEqual(2, "2")).toBeTruthy();
    expect(deepEqual(null, "")).toBeTruthy();
    expect(deepEqual("false", false)).toBeTruthy();
    expect(deepEqual("false", false)).toBeTruthy();

    const d = new Date(2020, 3, 12);
    expect(deepEqual(d, '2020-04-12')).toBeTruthy();
    expect(deepEqual(d, '2020-04-12', { isStrongVal: true })).toBeFalsy();

    expect(deepEqual({ a: "2", b: { s: 23, t: ["2", "3"] } }, { a: "2", b: { s: 23, t: ["2", "3"] } })).toBeTruthy();
    expect(deepEqual({ a: "2", b: { s: "23", t: [2, 3] } }, { a: 2, b: { s: 23, t: ["2", "3"] } })).toBeTruthy();
    expect(deepEqual({ a: "2", b: { s: "23", t: [2, 3] } }, { a: 2, b: { s: 23, t: ["2", "3"] } }, { isStrongVal: true })).toBeFalsy();

    expect(deepEqual([1, 2], [1, 3])).toBeFalsy();
    expect(deepEqual([1, 2], [1, 2])).toBeTruthy();

    expect(deepEqual({ a: 2 }, { a: "2" })).toBeTruthy();

    expect(deepEqual({ a: 2 }, { a: "2", f: "33" })).toBeFalsy();
    expect(deepEqual({ a: "2", f: "33" }, { a: 2 })).toBeTruthy();

    expect(deepEqual({ a: "2", f: "33" }, { a: 2 })).toBeTruthy();
    expect(deepEqual({ a: "2", f: "33" }, { a: 2 }, { isStrongKey: true })).toBeFalsy();

    expect(deepEqual(["1", "2"], ["1"])).toBeTruthy();
    expect(deepEqual(["1", "2"], ["1"], { isStrongKey: true })).toBeFalsy();
  });
});



describe('search', () => {
  it('works', () => {

    const list1 = ["Пар", "Кра па", "ФФ", "Ва пА пА"];
    expect(search(list1, "ПА")).toEqual(['Пар', 'Кра па', 'Ва пА пА']);

    const list2 = [{ f2: "Пар" }, { f: "Кра па" }, { f: "ФФ" }, { f: "Ва пА пА" }];
    expect(search(list2, "ПА", "f")).toEqual([{ f: 'Кра па' }, { f: 'Ва пА пА' }]);

    const list3 = [{ f2: { f: "Пар" } }, { f2: { f: "Кра па" } }, { f: "ФФ" }, { f: "Ва пА пА" }];
    expect(search(list3, "ПА", "f2.f")).toEqual([{ f2: { f: "Пар" } }, { f2: { f: "Кра па" } }]);
  });
});



describe('sortObjectKeys', () => {
  it('sort on keys', () => {

    const sortObj = sortObjectKeys({
      c: 1,
      b: 1,
      d: 1,
      a: 1,
    }, { keys: ['b', 'a', 'c'] });



    expect(JSON.stringify(sortObj)).toEqual(JSON.stringify({
      b: 1,
      a: 1,
      c: 1,
      d: 1
    }));
  });

  it('sort ONLY keys', () => {

    const sortObj1 = sortObjectKeys({
      c: 1,
      b: 1,
      d: 1,
      a: 1,
    }, { keys: ['b', 'a', 'c'], onlyKeys: true });

    expect(JSON.stringify(sortObj1)).toEqual(JSON.stringify({
      b: 1,
      a: 1,
      c: 1
    }));
  });

  it('sort on Func', () => {
    function sortFn(keyA: string, keyB: string) {
      let a = parseInt(keyA.slice(4));
      let b = parseInt(keyB.slice(4));
      return a - b;
    }


    expect(JSON.stringify(sortObjectKeys({
      "key-1": 1,
      "key-3": 1,
      "key-10": 1,
      "key-2": 1,
    }, { sortFn }))).toEqual(JSON.stringify({
      "key-1": 1,
      "key-2": 1,
      "key-3": 1,
      "key-10": 1,
    }));
  });
});


describe('kebab_case', () => {
  it('works', () => {
    expect(kebab_case("person2FieldInput")).toEqual("person2-field-input");
  });
});



describe('diff', () => {
  it('works', () => {

    const order1 = {
      sandwich: 'tuna',
      chips: true,
      drink: 'soda',
      order: 1,
      toppings: ['pickles', 'mayo', 'lettuce'],
      details: {
        name: 'Chris',
        phone: '555-555-5555',
        email: 'no@thankyou.com'
      },
      otherVal1: '1',
      f: null
    };

    const order2 = {
      sandwich: 'turkey',
      chips: true,
      drink: 'soda',
      order: 2,
      toppings: ['pickles', 'lettuce'],
      details: {
        name: 'Jon',
        phone: '(555) 555-5555',
        email: 'yes@please.com'
      },
      otherVal2: '2',
      f: undefined
    };

    const d2 = diff(order1, order2);
    // console.log(d2);

    expect(d2).toEqual({
      "sandwich": "turkey",
      "order": 2,
      "toppings": [
        "pickles",
        "lettuce"
      ],
      "details": {
        "name": "Jon",
        "phone": "(555) 555-5555",
        "email": "yes@please.com"
      },
      "otherVal1": null,
      "otherVal2": "2",
      f: undefined
    });



  });
});


describe('flatObject', () => {
  it('works', () => {

    const input = {
      Key1: '1',
      Key2: {
        a: '2',
        b: '3',
        c: {
          d: ['3', { gg: '10' }],
          e: '1',
        },
      },
    };


    expect(flatObject(input)).toEqual({
      "Key1": "1",
      "Key2.a": "2",
      "Key2.b": "3",
      "Key2.c.d.0": "3",
      "Key2.c.d.1.gg": "10",
      "Key2.c.e": "1"
    });
  });
});


describe('unflatObject', () => {
  it('works', () => {

    const input = {
      "Key1": "1",
      "Key2.a": "2",
      "Key2.b": "3",
      "Key2.c.d.0": "3",
      "Key2.c.d.1.gg": "10",
      "Key2.c.e": "1"
    };


    expect(unflatObject(input)).toEqual({
      Key1: '1',
      Key2: {
        a: '2',
        b: '3',
        c: {
          d: ['3', { gg: '10' }],
          e: '1',
        },
      },
    });
  });
});


describe('getId', () => {
  it('getId', () => {

    expect(getId({ id: "1" })).toEqual("1");
    expect(getId({ Id: "1" })).toEqual("1");

    expect(getId({ keyId: "1" }, { defId: "keyId" })).toEqual("1");
    expect(getId({ KeyId: "1" }, { defId: "keyId" })).toEqual("1");
    expect(getId({ key_id: "1" }, { defId: "keyId" })).toEqual("1");

    expect(getId({ fooId: "1" }, { entityType: "foo" })).toEqual("1");
    expect(getId({ foo_id: "1" }, { entityType: "foo" })).toEqual("1");

    expect(getId({ fooId: "1" }, { entityType: "some.foo" })).toEqual("1");
    expect(getId({ foo_id: "1" }, { entityType: "some.foo" })).toEqual("1");

  });
});