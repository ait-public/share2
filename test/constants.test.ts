import { constants } from '../src';


describe('dict2.address_source.code4001', () => {
  it('works', () => {
    expect(constants.dict2.address_source.code4001).toEqual("00000000-0000-0000-0040-000000000001");
  });
});

